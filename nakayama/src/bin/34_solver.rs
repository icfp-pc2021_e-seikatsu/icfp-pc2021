use std::env::args;
use std::fs::File;
use std::io::{BufRead, BufReader};

use itertools::iproduct;

use omu_solver::{
    input::read_problem,
    model::{Problem, Solution},
};

fn main() {
    let path = args().nth(1).expect("./bruteforce.rs json-path");
    let problem = read_problem(path).unwrap();

    let path = args().nth(2).expect("input");
    let f = BufReader::new(File::open(path).unwrap());
    for line in f.lines() {
        let s: Solution = serde_json::from_str(&line.unwrap()).unwrap();
        if let Some(s) = run(&problem, s) {
            println!("{}", serde_json::to_string(&s).unwrap());
        }
    }
}

fn run(problem: &Problem, mut s: Solution) -> Option<Solution> {
    for (x0, x1, y0, y1) in iproduct!(0..100, 0..100, 0..100, 0..100) {
        if (x1, y0, y1) == (0, 0, 0) {
            eprintln!("{}", x0);
        }
        s.vertices[4].0 = x0;
        s.vertices[5].0 = x1;
        s.vertices[4].1 = y0;
        s.vertices[5].1 = y1;
        if s.score(problem) == 0 {
            return Some(s);
        }
    }
    None
}
