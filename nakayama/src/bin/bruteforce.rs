//! 点の数が hole >= figure 同じ場合に全探索する
use nakayama::{Ans, Input};
use permutohedron::LexicalPermutation;
use std::env::args;
use std::fs::File;
use std::process::exit;

const D: i128 = 1_000_000;

fn main() {
    let path = args().nth(1).expect("./bruteforce.rs json-path");
    let input: Input = {
        let f = File::open(path).expect("fail to open");
        serde_json::from_reader(f).expect("fail to parse")
    };
    assert!(input.figure.vertices.len() <= input.hole.len());

    let mut original_d2 = Vec::with_capacity(input.figure.edges.len());
    for &(s, t) in input.figure.edges.iter() {
        original_d2.push(input.figure.vertices[s].dist(input.figure.vertices[t]));
    }

    let epsilon2 = (input.epsilon * input.epsilon) as i128;
    let mut vertices_map = (0..input.hole.len()).collect::<Vec<_>>();
    loop {
        let mut ok = true;
        for (i, &(s, t)) in input.figure.edges.iter().enumerate() {
            let s = vertices_map[s];
            let t = vertices_map[t];
            // | d / od - 1 | <= e / D  (D=10^6)
            // => D * | d - od | <= e * od
            // => D^2 * (d^2 - 2 * d * od + od^2) <= e^2 * od^2
            // => (D^2 * (d^2 + od^2) - e^2 * od^2)^2 <= 4 * D^4 * d^2 * od^2
            let d2 = input.hole[s].dist(input.hole[t]);
            let a = (d2 + original_d2[i]) as i128 * D * D - epsilon2 * original_d2[i] as i128;
            let b2 = 4 * D * D * D * D * (d2 * original_d2[i]) as i128;
            if a * a > b2 {
                ok = false;
                break;
            }
        }
        if ok {
            let mut vertices = Vec::with_capacity(vertices_map.len());
            for i in vertices_map {
                vertices.push(input.hole[i]);
            }
            println!(
                "{}",
                serde_json::to_string(&Ans {
                    vertices,
                    bonuses: Vec::new()
                })
                .unwrap()
            );
            break;
        }
        if !vertices_map.next_permutation() {
            exit(1);
        }
    }
}
