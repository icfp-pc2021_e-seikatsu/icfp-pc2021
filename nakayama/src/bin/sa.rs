//! 焼なまし
use std::fs::File;

use rand::Rng;
use serde::Deserialize;
use structopt::StructOpt;

use nakayama::{contains_point_in_polygon, Ans, Coord, Input, Line, Point, UseBonus};

struct Board {
    epsilon: f64,
    hole_vertices: Vec<Point>,
    hole_lines: Vec<Line>,
    original_dist: Vec<Vec<Coord>>,
}

impl Board {
    fn new(input: &Input) -> Board {
        let mut hole_lines = Vec::with_capacity(input.hole.len());
        for i in 0..input.hole.len() {
            hole_lines.push(Line::new(
                input.hole[i],
                input.hole[(i + 1) % input.hole.len()],
            ));
        }

        let mut original_dist = Vec::with_capacity(input.figure.edges.len());
        for _ in 0..input.figure.edges.len() {
            original_dist.push(Vec::new());
        }
        for &(s, t) in input.figure.edges.iter() {
            let d = input.figure.vertices[s].dist(input.figure.vertices[t]);
            original_dist[s].push(d);
            original_dist[t].push(d);
        }

        Board {
            epsilon: input.epsilon.max(1) as f64 / 1_000_000.0,
            hole_vertices: input.hole.clone(),
            hole_lines,
            original_dist,
        }
    }
}

struct SA {
    edge: Vec<Vec<usize>>,
    vertices: Vec<Point>,
    cost: f64,
    hole_cost: Coord,
    best_cost: i64,
    best_vertices: Vec<Point>,
}

trait EdgeErrorCost {
    fn cals_local_diff(&mut self, board: &Board, sa: &SA, s: usize, point: Point) -> f64;

    fn accept(&mut self);

    fn calc_all(&mut self, board: &Board, sa: &SA) -> f64;
}

struct LocalEdgeErrorCost {
    cost: Vec<f64>,
    last_index: usize,
    last_cost: f64,
}

struct GlobalEdgeErrorCost {
    total_d: f64,
    local_d: Vec<f64>,
    last_index: usize,
    last_d: f64,
}

impl LocalEdgeErrorCost {
    #[allow(dead_code)]
    fn new(n: usize) -> LocalEdgeErrorCost {
        LocalEdgeErrorCost {
            cost: vec![0.0; n],
            last_index: 0,
            last_cost: 0.0,
        }
    }

    fn calc_local(board: &Board, sa: &SA, s: usize, point: Point) -> f64 {
        let mut error = 0.0;
        for (&t, &od) in sa.edge[s].iter().zip(board.original_dist[s].iter()) {
            let d = (1.0 - point.dist(sa.vertices[t]) as f64 / od as f64).abs();
            if d > board.epsilon {
                error += d / board.epsilon;
            }
        }
        error
    }
}

impl EdgeErrorCost for LocalEdgeErrorCost {
    fn cals_local_diff(&mut self, board: &Board, sa: &SA, s: usize, point: Point) -> f64 {
        let new = Self::calc_local(board, sa, s, point);
        self.last_index = s;
        self.last_cost = new;
        new - self.cost[s]
    }

    fn accept(&mut self) {
        self.cost[self.last_index] = self.last_cost;
    }

    fn calc_all(&mut self, board: &Board, sa: &SA) -> f64 {
        let mut error = 0.0;
        for (s, &point) in sa.vertices.iter().enumerate() {
            self.cost[s] = Self::calc_local(board, sa, s, point);
            error += self.cost[s];
        }
        error / 2.0
    }
}

impl GlobalEdgeErrorCost {
    #[allow(dead_code)]
    fn new(n: usize) -> GlobalEdgeErrorCost {
        GlobalEdgeErrorCost {
            total_d: 0.0,
            local_d: vec![0.0; n],
            last_index: 0,
            last_d: 0.0,
        }
    }

    fn calc_local(board: &Board, sa: &SA, s: usize, point: Point) -> f64 {
        let mut d = 0.0;
        for (&t, &od) in sa.edge[s].iter().zip(board.original_dist[s].iter()) {
            d += (1.0 - point.dist(sa.vertices[t]) as f64 / od as f64).abs();
        }
        d
    }

    fn error(board: &Board, sa: &SA, d: f64) -> f64 {
        let e = board.epsilon * sa.vertices.len() as f64;
        if d > e {
            d / e
        } else {
            1.0
        }
    }
}

impl EdgeErrorCost for GlobalEdgeErrorCost {
    fn cals_local_diff(&mut self, board: &Board, sa: &SA, s: usize, point: Point) -> f64 {
        let d = Self::calc_local(board, sa, s, point);
        self.last_index = s;
        self.last_d = d;
        let diff = d - self.local_d[s];
        let old = Self::error(board, sa, self.total_d);
        let new = Self::error(board, sa, self.total_d + diff);
        new - old
    }

    fn accept(&mut self) {
        self.local_d[self.last_index] = self.last_d;
    }

    fn calc_all(&mut self, board: &Board, sa: &SA) -> f64 {
        let mut d = 0.0;
        for (s, &point) in sa.vertices.iter().enumerate() {
            self.local_d[s] = Self::calc_local(board, sa, s, point);
            d += self.local_d[s];
        }
        Self::error(board, sa, d / 2.0)
    }
}

impl SA {
    fn new(input: &Input, init: Point) -> SA {
        let mut edge = Vec::with_capacity(input.figure.vertices.len());
        for _ in 0..input.figure.vertices.len() {
            edge.push(Vec::with_capacity(1));
        }
        for &(s, t) in input.figure.edges.iter() {
            edge[s].push(t);
            edge[t].push(s);
        }
        SA {
            edge,
            vertices: vec![init; input.figure.vertices.len()],
            cost: 0.0,
            hole_cost: 0,
            best_cost: i64::MAX,
            best_vertices: Vec::new(),
        }
    }

    fn set_init(&mut self, init: Point) {
        self.vertices = vec![init; self.vertices.len()];
    }

    fn is_edge_valid(&self, board: &Board, s: usize, new_point: Point) -> bool {
        if !contains_point_in_polygon(new_point, &board.hole_vertices) {
            return false;
        }
        for &t in self.edge[s].iter() {
            let line = Line::new(new_point, self.vertices[t]);
            for hole in board.hole_lines.iter() {
                if line.is_cross(hole) {
                    return false;
                }
            }
        }
        true
    }

    fn calc_hole_cost(&self, board: &Board) -> Coord {
        let mut cost = 0;
        for &h in board.hole_vertices.iter() {
            let m = self.vertices.iter().map(|v| v.dist(h)).min().unwrap();
            cost += m;
        }
        cost
    }

    fn update_all_cost<E: EdgeErrorCost>(
        &mut self,
        board: &Board,
        error_weight: f64,
        edge_error: &mut E,
    ) {
        self.hole_cost = self.calc_hole_cost(board);
        self.cost = edge_error.calc_all(board, self) * error_weight + self.hole_cost as f64;
    }

    fn mmc<R: Rng, E: EdgeErrorCost>(
        &mut self,
        rng: &mut R,
        board: &Board,
        beta: f64,
        error_weight: f64,
        edge_error: &mut E,
        bonuses: &[UseBonus],
    ) {
        let v_index = rng.gen_range(0..self.vertices.len());
        let p = self.vertices[v_index];
        let mut q = self.vertices[v_index];
        while q == self.vertices[v_index] {
            q.0 += rng.gen_range(-1..=1);
            q.1 += rng.gen_range(-1..=1);
        }
        if !self.is_edge_valid(board, v_index, q) {
            return;
        }
        let error_cost_diff = edge_error.cals_local_diff(board, self, v_index, p);
        self.vertices[v_index] = q;
        let hole_cost_diff = self.calc_hole_cost(board) - self.hole_cost;
        let cost_diff = error_cost_diff * error_weight + hole_cost_diff as f64;
        if cost_diff < 0.0 || rng.gen_bool((-cost_diff * beta).exp()) {
            self.hole_cost += hole_cost_diff;
            self.cost += cost_diff;
            edge_error.accept();
            let cost = self.cost.round() as i64;
            if cost < self.best_cost && edge_error.calc_all(board, self) == 0.0 {
                self.best_cost = cost;
                self.best_vertices = self.vertices.clone();
                println!("{}", self.best_cost);
                println!(
                    "{}",
                    serde_json::to_string(&Ans {
                        vertices: self.best_vertices.clone(),
                        bonuses: bonuses.to_vec(),
                    })
                    .unwrap()
                );
            }
        } else {
            self.vertices[v_index] = p;
        }
    }
}

#[derive(Debug, Deserialize)]
struct Best {
    problem: u32,
    your: Option<i64>,
}

fn find_best(problem: u32) -> Option<i64> {
    let best_data = include_str!("../../best.txt");
    let mut best = csv::ReaderBuilder::new()
        .delimiter(b'\t')
        .from_reader(best_data.as_bytes());
    for result in best.deserialize() {
        let record: Best = result.unwrap();
        if record.problem == problem {
            return record.your;
        }
    }
    None
}

#[derive(Debug, StructOpt)]
struct Opt {
    problem_number: u32,
    #[structopt(long)]
    init_path: Option<String>,
    #[structopt(short = "g", long)]
    globalist: Option<u32>,
}

fn main() {
    let opt: Opt = Opt::from_args();
    let best = find_best(opt.problem_number);
    if best == Some(0) {
        return;
    }

    let input: Input = {
        let path = format!("./problems/{}.json", opt.problem_number);
        let f = File::open(path).expect("fail to open");
        serde_json::from_reader(f).expect("fail to parse")
    };

    let init = if let Some(ref path) = opt.init_path {
        let f = File::open(path).expect("fail to open");
        let a: Ans = serde_json::from_reader(f).expect("fail to parse");
        Some(a)
    } else {
        None
    };

    let board = Board::new(&input);
    let mut mc = SA::new(&input, Point(0, 0));
    mc.best_cost = best.unwrap_or(i64::MAX);
    let mut bonuses = Vec::new();
    if let Some(problem) = opt.globalist {
        let edge_error = GlobalEdgeErrorCost::new(input.figure.vertices.len());
        bonuses.push(UseBonus {
            problem,
            bonus: "GLOBALIST".to_owned(),
        });
        run(input, init, board, mc, edge_error, &bonuses);
    } else {
        let edge_error = LocalEdgeErrorCost::new(input.figure.vertices.len());
        run(input, init, board, mc, edge_error, &bonuses);
    }
}

fn run<E: EdgeErrorCost>(
    input: Input,
    init: Option<Ans>,
    board: Board,
    mut mc: SA,
    mut edge_error: E,
    bonuses: &[UseBonus],
) {
    let mut rng = rand_pcg::Mcg128Xsl64::new(999);
    loop {
        if let Some(ref init) = init {
            mc.vertices = init.vertices.clone();
        } else {
            let init = loop {
                let x = rng.gen_range(0..100);
                let y = rng.gen_range(0..100);
                if contains_point_in_polygon(Point(x, y), &board.hole_vertices) {
                    break Point(x, y);
                }
            };
            mc.set_init(init);
        };

        let max_step = 1_000_000;
        let t_max: f64 = rng.gen_range(10.0..10_000.0);
        let t_min: f64 = 0.01;
        let error_weight_min: f64 = rng.gen_range(0.0..100.0);
        let error_weight_max: f64 = rng.gen_range(10.0..1000.0);
        for i in 0..max_step {
            let r = i as f64 / max_step as f64;
            let t = t_max.powf(1.0 - r) * t_min.powf(r);
            let beta = 1.0 / t;
            let error_weight = error_weight_min.powf(1.0 - r) * error_weight_max.powf(r);
            mc.update_all_cost(&board, error_weight, &mut edge_error);
            for _ in 0..input.figure.vertices.len() {
                mc.mmc(
                    &mut rng,
                    &board,
                    beta,
                    error_weight,
                    &mut edge_error,
                    bonuses,
                );
            }
        }
    }
}
