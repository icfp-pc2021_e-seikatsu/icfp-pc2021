use std::collections::HashMap;
use std::env::args;
use std::fs::File;

use nakayama::{Ans, Input};

struct Board {
    input: Input,
    hole_edge_range: Vec<Vec<(i64, i64)>>,
    edges: Vec<HashMap<usize, i64>>,
}

impl Board {
    fn new(input: &Input) -> Board {
        let n = input.hole.len();
        let mut hole_edge_range = Vec::with_capacity(n);
        for _ in 0..n {
            hole_edge_range.push(vec![(0, 0); n]);
        }
        for i in 0..n {
            for j in i + 1..n {
                let mut min = -1;
                let mut max = i64::MAX;
                let d = input.hole[i].dist(input.hole[j]);
                for od in 1..=d {
                    if (d - od).abs() * 1_000_000 <= input.epsilon as i64 * od {
                        min = od;
                        break;
                    }
                }
                for od in d + 1.. {
                    if (d - od).abs() * 1_000_000 > input.epsilon as i64 * od {
                        max = od - 1;
                        break;
                    }
                }
                hole_edge_range[i][j] = (min, max);
                hole_edge_range[j][i] = (min, max);
            }
        }

        let n = input.figure.edges.len();
        let mut edges = Vec::with_capacity(n);
        for _ in 0..n {
            edges.push(HashMap::with_capacity(2));
        }
        for &(s, t) in input.figure.edges.iter() {
            let d = input.figure.vertices[s].dist(input.figure.vertices[t]);
            edges[s].insert(t, d);
            edges[t].insert(s, d);
        }

        Board {
            input: input.clone(),
            hole_edge_range,
            edges,
        }
    }
}

fn main() {
    let path = args().nth(1).expect("./bruteforce.rs json-path");
    let input: Input = {
        let f = File::open(path).expect("fail to open");
        serde_json::from_reader(f).expect("fail to parse")
    };
    assert!(input.figure.vertices.len() >= input.hole.len());

    let board = Board::new(&input);
    let mut path = Vec::new();
    let mut used = vec![false; input.figure.vertices.len()];
    for s in 0..input.figure.vertices.len() {
        used[s] = true;
        path.push(s);
        dfs(&board, &mut path, &mut used);
        path.pop();
        used[s] = false;
    }
}

fn dfs(board: &Board, path: &mut Vec<usize>, used: &mut [bool]) {
    if board.hole_edge_range.len() == path.len() {
        dump(&board.input, path);
        return;
    }
    let j = path.len();
    'OUT: for t in 0..used.len() {
        if used[t] {
            continue;
        }
        let m = &board.edges[t];
        for (i, s) in path.iter().enumerate() {
            if let Some(&d) = m.get(s) {
                let (min, max) = board.hole_edge_range[j][i];
                if d < min || max < d {
                    continue 'OUT;
                }
            }
        }
        used[t] = true;
        path.push(t);
        dfs(board, path, used);
        path.pop();
        used[t] = false;
    }
}

fn dump(input: &Input, path: &[usize]) {
    let mut vertices = vec![nakayama::Point(-1, -1); input.figure.vertices.len()];
    for (i, &p) in path.iter().enumerate() {
        vertices[p] = input.hole[i];
    }
    let ans = Ans {
        vertices,
        bonuses: Vec::new(),
    };
    println!("{}", serde_json::to_string(&ans).unwrap());
}
