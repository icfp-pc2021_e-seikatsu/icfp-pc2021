use serde::{Deserialize, Serialize};
use std::cmp::Ordering;

pub type Coord = i64;

#[derive(Debug, Deserialize, Serialize, Copy, Clone, Eq, PartialEq)]
pub struct Point(pub Coord, pub Coord);

impl PartialOrd for Point {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        if self.0 == other.0 {
            return self.1.partial_cmp(&other.1);
        }
        return self.0.partial_cmp(&other.0);
    }
}

impl Ord for Point {
    fn cmp(&self, other: &Self) -> Ordering {
        if self.0 == other.0 {
            return self.1.cmp(&other.1);
        }
        return self.0.cmp(&other.0);
    }
}

#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct Input {
    pub hole: Vec<Point>,
    pub epsilon: u32,
    pub figure: Figure,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct Figure {
    pub edges: Vec<(usize, usize)>,
    pub vertices: Vec<Point>,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct UseBonus {
    pub bonus: String,
    pub problem: u32,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct Ans {
    pub vertices: Vec<Point>,
    pub bonuses: Vec<UseBonus>,
}

impl Point {
    pub fn dist(self, other: Point) -> Coord {
        let dx = self.0 - other.0;
        let dy = self.1 - other.1;
        dx * dx + dy * dy
    }
}

#[derive(Debug, Clone)]
pub struct Line {
    pub a: Point,
    pub b: Point,
}

impl Line {
    pub fn new(a: Point, b: Point) -> Line {
        Line { a, b }
    }

    /// 端点を含まない線分同士の交差判定
    /// https://qiita.com/ykob/items/ab7f30c43a0ed52d16f2
    pub fn is_cross(&self, other: &Line) -> bool {
        let ta = (other.a.0 - other.b.0) * (self.a.1 - other.a.1)
            + (other.a.1 - other.b.1) * (other.a.0 - self.a.0);
        let tb = (other.a.0 - other.b.0) * (self.b.1 - other.a.1)
            + (other.a.1 - other.b.1) * (other.a.0 - self.b.0);
        let tc = (self.a.0 - self.b.0) * (other.a.1 - self.a.1)
            + (self.a.1 - self.b.1) * (self.a.0 - other.a.0);
        let td = (self.a.0 - self.b.0) * (other.b.1 - self.a.1)
            + (self.a.1 - self.b.1) * (self.a.0 - other.b.0);
        ta * tb < 0 && tc * td < 0
    }
}

pub fn contains_point_in_polygon(point: Point, polygon: &[Point]) -> bool {
    type Float = f64;
    let mut wn: i32 = 0;

    for &p in polygon {
        if p == point {
            return true;
        }
    }

    for idx in 0..polygon.len() {
        let coordinate = &polygon[idx];
        let x0 = coordinate.0 as Float;
        let y0 = coordinate.1 as Float;

        let jdx = (idx + 1) % polygon.len();

        let coordinate = &polygon[jdx];
        let x1 = coordinate.0 as Float;
        let y1 = coordinate.1 as Float;

        let tx = point.0 as Float;
        let ty = point.1 as Float;

        if y0 <= ty && y1 > ty {
            let vt = (ty - y0) / (y1 - y0);
            if tx < (x0 + (vt * (x1 - x0))) {
                wn += 1;
            }
        } else if y0 > ty && y1 <= ty {
            let vt = (ty - y0) / (y1 - y0);
            if tx < (x0 + (vt * (x1 - x0))) {
                wn -= 1;
            }
        }
    }

    wn.abs() % 2 == 1
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_data_parse() {
        let data = include_str!("../../problems/1.json");
        let input: Input = serde_json::from_reader(data.as_bytes()).unwrap();
        assert_eq!(input.epsilon, 150000);
    }

    #[test]
    fn test_dist() {
        assert_eq!(Point(10, 12).dist(Point(13, 16)), 25);
    }

    #[test]
    fn is_cross_0() {
        let a = Line::new(Point(1, 3), Point(5, 1));
        // 端点は含まない
        assert!(!a.is_cross(&Line::new(Point(1, 0), Point(1, 3))));
        assert!(!a.is_cross(&Line::new(Point(1, 0), Point(1, 4))));
        assert!(!a.is_cross(&Line::new(Point(0, 0), Point(5, 1))));
        assert!(!a.is_cross(&Line::new(Point(4, 5), Point(3, 2))));
        // 交差
        assert!(a.is_cross(&Line::new(Point(1, 0), Point(2, 3))));
        assert!(a.is_cross(&Line::new(Point(0, 0), Point(6, 2))));
        assert!(a.is_cross(&Line::new(Point(2, 100), Point(3, 1))));
        assert!(a.is_cross(&Line::new(Point(6, 0), Point(0, 4))));
    }

    #[test]
    fn is_cross_1() {
        let a = Line::new(Point(2, 2), Point(10, 2));
        // 重なった線分は交差ではない
        assert!(!a.is_cross(&a));
        assert!(!a.is_cross(&Line::new(Point(2, 2), Point(9, 2))));
        assert!(!a.is_cross(&Line::new(Point(3, 2), Point(9, 2))));
        assert!(!a.is_cross(&Line::new(Point(3, 2), Point(11, 2))));
        assert!(!a.is_cross(&Line::new(Point(1, 2), Point(11, 2))));
        assert!(!a.is_cross(&Line::new(Point(1, 2), Point(9, 2))));
    }
}
