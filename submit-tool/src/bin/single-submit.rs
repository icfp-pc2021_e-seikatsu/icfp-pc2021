use std::io::{stdin, Read};

use anyhow::{Context, Result};
use structopt::StructOpt;

use omu_solver::input::{read_problem, read_solution};
use submit_tool::{now, problems_path, save_solution, submit};

#[derive(Debug, StructOpt)]
struct Opt {
    name: String,
    #[structopt(short = "n", long)]
    problem_number: u32,
    #[structopt(short = "s", long)]
    solution_path: Option<String>,
    #[structopt(long)]
    force: bool,
}

#[tokio::main]
async fn main() -> Result<()> {
    let opt = Opt::from_args();

    let path = format!("{}.json", opt.problem_number);
    let problem_path = problems_path().join(&path);
    let problem =
        read_problem(problem_path).context(format!("fail to open problem file: {}", path))?;

    let mut solution = match opt.solution_path {
        Some(path) => {
            read_solution(&path).context(format!("fail to open solution file: {}", path))?
        }
        None => {
            let stdin = stdin();
            let mut cin = stdin.lock();
            println!("答えのJSONを貼る(Ctrl+Dが要るかも)");
            let mut buf = String::new();
            cin.read_to_string(&mut buf).context("fait to read stdin")?;
            serde_json::from_str(&buf).context(format!("fait to parse json: {:?}", buf))?
        }
    };

    let score = solution.score(&problem);
    println!("予測スコア: {}", score);
    if score < 0 && !opt.force {
        println!("駄目そう。強制提出するなら --force をつける");
        return Ok(());
    }

    let r = submit(opt.problem_number, &solution)
        .await
        .context("fail to submit")?;
    println!("submit result: {:?}", r);

    solution.problem = Some(opt.problem_number);
    solution.time = Some(now());
    solution.solver = Some(opt.name);
    solution.score = Some(score);
    solution.submit_id = Some(r.id);

    save_solution(&solution).context("fail to save solution")?;

    Ok(())
}
