use std::env::args;
use std::path::PathBuf;

use anyhow::Context;
use serde::{Deserialize, Serialize};

use omu_solver::model::Solution;

pub fn repo_root_path() -> PathBuf {
    let path = PathBuf::from(args().next().unwrap());
    path.parent()
        .unwrap()
        .parent()
        .unwrap()
        .parent()
        .unwrap()
        .to_owned()
}

pub fn save_solution(solution: &Solution) -> anyhow::Result<()> {
    use std::io::Write;
    let path = repo_root_path()
        .join("solutions")
        .join(format!("{}", solution.problem.unwrap()));
    if !path.is_dir() {
        std::fs::create_dir(&path).context(format!("fail to mkdir: {:?}", path))?;
    }
    let path = path.join(format!("{}.json", solution.time.as_ref().unwrap()));
    let mut f = std::fs::File::create(&path).context(format!("fail to create file: {:?}", path))?;
    write!(f, "{}", serde_json::to_string(solution).unwrap()).context("fail to write")?;
    Ok(())
}

pub fn problems_path() -> PathBuf {
    repo_root_path().join("problems")
}

pub fn now() -> String {
    use chrono::{Duration, Utc};
    let dt = Utc::now().checked_add_signed(Duration::hours(9)).unwrap();
    dt.format("%Y%m%d_%H%M%S").to_string()
}

#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct SubmitResponse {
    pub id: String,
    #[serde(flatten)]
    pub other: serde_json::Value,
}

pub async fn submit(problem_number: u32, solution: &Solution) -> anyhow::Result<SubmitResponse> {
    let client = reqwest::Client::new();
    let res = client
        .post(format!(
            "https://poses.live/api/problems/{}/solutions",
            problem_number
        ))
        .header(
            "Authorization",
            "Bearer 7ec48e69-ddb3-43d3-95c5-54396b198224",
        )
        .json(solution)
        .send()
        .await
        .context("fail to send solutions")?;
    if !res.status().is_success() {
        Err(anyhow::anyhow!("リクエスト失敗"))
    } else {
        Ok(res.json().await.context("fail to parse response")?)
    }
}
