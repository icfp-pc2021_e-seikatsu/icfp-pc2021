use actix_cors::Cors;
use actix_web::{get, middleware::Logger, post, web, App, HttpResponse, HttpServer, Responder};
use serde_derive::{Deserialize, Serialize};

use omu_solver::input::{read_problem, read_solution};
use omu_solver::model::{Problem, Solution, Vertices};

#[derive(Serialize, Deserialize, Debug, Clone)]
struct Submit {
    vertices: Vertices,
    score: i64,
    name: String,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
struct GetProblemResponse {
    problem_id: u32,
    problem: Problem,
    submits: Vec<Submit>,
    minimal: Option<i64>,
}

#[derive(Debug, Deserialize)]
struct Best {
    problem: u32,
    minimal: Option<i64>,
}

fn find_best(problem: u32) -> Option<i64> {
    let best_data = include_str!("../../nakayama/best.txt");
    let mut best = csv::ReaderBuilder::new()
        .delimiter(b'\t')
        .from_reader(best_data.as_bytes());
    for result in best.deserialize() {
        let record: Best = result.unwrap();
        if record.problem == problem {
            return record.minimal;
        }
    }
    None
}

#[post("/problems/{problem_id}/solutions")]
async fn post_solution(
    web::Path(problem_id): web::Path<u32>,
    solution: web::Json<Solution>,
) -> impl Responder {
    let problem_path = format!("../problems/{}.json", problem_id);
    if let Ok(problem) = read_problem(problem_path) {
        let score = solution.score(&problem);
        return HttpResponse::Ok().json(PostSolutionResponse { score });
    }
    HttpResponse::NotFound().finish()
}

#[get("/problems/{problem_id}")]
async fn get_problem(web::Path(problem_id): web::Path<u32>) -> impl Responder {
    let problem_path = format!("../problems/{}.json", problem_id);
    let mut submits = vec![];

    let problem = read_problem(problem_path).unwrap();

    if let Ok(dir) = std::fs::read_dir(format!("../solutions/{}", problem_id)) {
        for path in dir {
            let path = path.unwrap();
            if path.file_type().unwrap().is_dir() {
                continue;
            }
            if let Ok(solution) = read_solution(path.path()) {
                let score = solution.score(&problem);
                submits.push(Submit {
                    vertices: solution.vertices,
                    score,
                    name: solution.solver.unwrap_or("unknown".to_owned()),
                });
                submits.sort_by(|a, b| a.score.cmp(&b.score));
            }
        }
    }

    HttpResponse::Ok().json(GetProblemResponse {
        problem_id,
        problem,
        submits,
        minimal: find_best(problem_id),
    })
}

#[derive(Serialize, Deserialize, Debug, Clone)]
struct PostSolutionBodyParam {
    solution: Solution,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
struct PostSolutionResponse {
    score: i64,
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    env_logger::init_from_env(env_logger::Env::new().default_filter_or("info"));

    HttpServer::new(|| {
        let cors = Cors::default()
            .allow_any_origin()
            .allow_any_method()
            .allow_any_header();

        App::new()
            .wrap(Logger::default())
            .wrap(cors)
            .service(get_problem)
            .service(post_solution)
    })
    .bind("0.0.0.0:8080")?
    .run()
    .await
}
