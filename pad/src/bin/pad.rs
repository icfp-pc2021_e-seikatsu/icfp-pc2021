use anyhow::Result;
use coffee::graphics::WindowSettings;
use coffee::ui::UserInterface;
use pad::config::{create_pad_config, PadConfig};
use pad::pad::PosesPad;
use structopt::StructOpt;

#[derive(StructOpt)]
struct Opt {
    #[structopt(short, long)]
    problem_path: String,
    #[structopt(short, long)]
    solution_path: Option<String>,
    #[structopt(short, long, default_value = "1024")]
    width: u32,
    #[structopt(short, long, default_value = "1024")]
    height: u32,
}

async fn pad(opt: Opt) -> Result<()> {
    create_pad_config(PadConfig::new(opt.problem_path, opt.solution_path))?;

    <PosesPad as UserInterface>::run(WindowSettings {
        title: String::from("Poses Pad"),
        size: (opt.width, opt.height),
        resizable: true,
        fullscreen: false,
        maximized: false,
    })?;
    Ok(())
}

#[tokio::main]
async fn main() -> Result<()> {
    let opt = Opt::from_args();
    pad(opt).await
}
