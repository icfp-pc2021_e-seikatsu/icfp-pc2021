use coffee::graphics::{
    Color, Frame, HorizontalAlignment, Mesh, Point, Rectangle, Shape, VerticalAlignment, Window,
};
use coffee::load::Task;
use coffee::ui::{
    button, slider, Align, Button, Column, Element, Justify, Radio, Renderer, Row, Slider, Text,
    UserInterface,
};
use coffee::{input, Game, Timer};

use crate::config::read_pad_config;
use coffee::input::{keyboard, mouse, Input};
use itertools::Itertools;
use omu_solver::geometry::{distance, distance_ps, intersect_ss};
use omu_solver::input::{read_problem, read_solution};
use omu_solver::model::{Bonus, Coordinate, Edge, Edges, Problem, Solution};
use omu_solver::solver::{
    fix_sa::{FixSASolver, FixSASolverArgument},
    sa::{NakayamaSASolver, NakayamaSASolverArgument},
    Solver,
};
use std::cmp::max;
use std::collections::HashSet;
use std::fs;
use std::ops::RangeInclusive;
use std::path::PathBuf;
use submit_tool::repo_root_path;
use sugihara::global::{GlobalSASolver, GlobalSASolverArgument};

#[derive(Clone, Copy)]
struct MagnificationState {
    mag: f32,
    x_offset: f32,
    y_offset: f32,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum SolverMode {
    NakayamaSA,
    GlobalSA,
    FixSA,
    Stop,
}

pub struct CustomInput {
    cursor_position: Point,
    mouse_wheel: Point,
    mouse_wheel_event_count: u32,
    keys_pressed: HashSet<keyboard::KeyCode>,
    mouse_buttons_pressed: HashSet<mouse::Button>,
    text_buffer: String,
}

impl Input for CustomInput {
    fn new() -> CustomInput {
        CustomInput {
            cursor_position: Point::new(0.0, 0.0),
            mouse_wheel: Point::new(0.0, 0.0),
            mouse_wheel_event_count: 0,
            keys_pressed: HashSet::new(),
            mouse_buttons_pressed: HashSet::new(),
            text_buffer: String::new(),
        }
    }

    fn update(&mut self, event: input::Event) {
        match event {
            input::Event::Mouse(mouse_event) => match mouse_event {
                mouse::Event::CursorMoved { x, y } => {
                    self.cursor_position = Point::new(x, y);
                }
                mouse::Event::Input { state, button } => match state {
                    input::ButtonState::Pressed => {
                        self.mouse_buttons_pressed.insert(button);
                    }
                    input::ButtonState::Released => {
                        self.mouse_buttons_pressed.remove(&button);
                    }
                },
                mouse::Event::WheelScrolled { delta_x, delta_y } => {
                    self.mouse_wheel = Point::new(delta_x, delta_y);
                    self.mouse_wheel_event_count += 1;
                    self.mouse_wheel_event_count %= 100;
                }
                _ => {}
            },
            input::Event::Keyboard(keyboard_event) => match keyboard_event {
                keyboard::Event::TextEntered { character } => {
                    self.text_buffer.push(character);
                }
                keyboard::Event::Input { key_code, state } => match state {
                    input::ButtonState::Pressed => {
                        self.keys_pressed.insert(key_code);
                    }
                    input::ButtonState::Released => {
                        self.keys_pressed.remove(&key_code);
                    }
                },
            },
            _ => {}
        }
    }

    fn clear(&mut self) {
        self.text_buffer.clear();
    }
}

struct Solvers {
    nakayama_sa_solver: NakayamaSASolver,
    global_sa_solver: GlobalSASolver,
    fix_sa_solver: FixSASolver,
}

impl Solvers {
    pub fn new(problem: &Problem, solution: &Solution) -> Self {
        Solvers {
            nakayama_sa_solver: NakayamaSASolver::new(&problem, &solution),
            global_sa_solver: GlobalSASolver::new(&problem, &solution),
            fix_sa_solver: FixSASolver::new(&problem, &solution),
        }
    }
}

#[derive(Eq, PartialEq, Copy, Clone)]
enum OperationMode {
    PointSelect,
    PointMove,
    PointMatch,
    RangeSelect,
    RangeMove,
    SegmentSelect,
    SegmentMove,
    SegmentMatch,
    DragWindow,
    HoleSegmentSelect,
    UseControlPanel,
}

#[derive(Eq, PartialEq, Copy, Clone)]
enum ViewMode {
    All,
    FilterEdgeDegree { degree: i32 },
}

pub struct PosesPad {
    problem: Problem,
    solution: Solution,
    score: i64,
    best_score: i64,
    original_score: i64,
    valid: bool,
    temperature: f32,
    error_weight: f32,
    cursor_position: Point,
    mouse_wheel: Point,
    keys_pressed: HashSet<keyboard::KeyCode>,
    mouse_buttons_pressed: HashSet<mouse::Button>,
    saved: bool,
    mag_state: MagnificationState,
    solvers: Solvers,
    viewer_x_max: f32,
    viewer_y_max: f32,

    degree_count_by_vertex: Vec<i32>,

    target_vertex_idx: usize,
    target_edge_idx: usize,
    target_hole_edge: Edge,

    operation_mode: OperationMode,
    prev_operation_mode: OperationMode,
    view_mode: ViewMode,

    range_select_begin_point: Point,
    range_select_points: Vec<Point>,
    range_select_vertex_indices: Vec<usize>,

    range_move_begin_point: Point,

    valid_hole_edges: Edges,
    valid_edge_indices: Vec<usize>,

    selected_segment: (Point, Point),
    segment_move_begin_point: Point,

    drag_window_begin_point: Point,
    drag_window_begin_mag_state: MagnificationState,

    mouse_wheel_event_count: u32,
    prev_mouse_wheel_event_count: u32,

    current_bonus: Option<Bonus>,
    original_solution: Solution,
    best_solution_path: Option<PathBuf>,
    panel: ControlPanel,
}

pub struct ControlPanel {
    solver_mode: SolverMode,
    temp_slider: slider::State,
    temp_buttons: DoubleHalveButtonsState,
    error_weight_slider: slider::State,
    error_buttons: DoubleHalveButtonsState,
}

pub struct DoubleHalveButtonsState {
    double_button: button::State,
    halve_button: button::State,
}

impl Game for PosesPad {
    type Input = CustomInput;
    type LoadingScreen = ();

    fn load(window: &Window) -> Task<PosesPad> {
        let config = read_pad_config().unwrap();
        let problem = read_problem(config.problem_path).unwrap();
        let mut solution = if let Some(solution_path) = config.solution_path {
            read_solution(solution_path).unwrap()
        } else {
            Solution::new(problem.figure.vertices.clone())
        };
        solution.solver = Some("poses-pad".to_string());

        let mut max_x = 0;
        let mut max_y = 0;

        for hole_pos in &problem.hole {
            max_x = max(max_x, hole_pos.0);
            max_y = max(max_y, hole_pos.1);
        }

        let width = window.width() / 2.;
        let height = window.height() - 100.;

        let x_mag = width / max_x as f32;
        let y_mag = height / max_y as f32;

        let mut score = solution.score(&problem);
        if score < 0 {
            score = i64::MAX;
        }
        let mag_state = MagnificationState {
            mag: if x_mag < y_mag { x_mag } else { y_mag },
            x_offset: 400.0,
            y_offset: 100.0,
        };

        let solutions_path = {
            let mut root_path = repo_root_path();
            root_path.push("solutions");
            root_path.push(problem.problem.to_string());
            root_path
        };

        let mut best_score = score;
        let mut best_solution_path = None;

        if let Ok(solutions_path) = fs::read_dir(solutions_path) {
            let solution_paths = solutions_path
                .filter_map(|entry| {
                    let path = entry.ok()?.path();
                    if path.is_file() {
                        Some(path)
                    } else {
                        None
                    }
                })
                .collect_vec();

            for solution_path in &solution_paths {
                if let Ok(solution) = read_solution(&solution_path) {
                    let score = solution.score(&problem);
                    if best_score > score {
                        best_score = score;
                        best_solution_path = Some(solution_path.clone());
                    }
                }
            }

            if let Some(best_solution_path) = &best_solution_path {
                println!("Found best solution: {:#?}", &best_solution_path);
            } else {
                best_score = score;
            }
        }

        let solvers = Solvers::new(&problem, &solution);

        let mut degree_count_by_vertex = vec![0; problem.figure.vertices.len()];

        for &edge in &problem.figure.edges {
            degree_count_by_vertex[edge.0] += 1;
            degree_count_by_vertex[edge.1] += 1;
        }

        Task::succeed(move || PosesPad {
            problem,
            solution: solution.clone(),
            cursor_position: Point::new(0.0, 0.0),
            mouse_wheel: Point::new(0.0, 0.0),
            keys_pressed: HashSet::new(),
            mouse_buttons_pressed: HashSet::new(),
            score,
            best_score,
            original_score: score,
            valid: true,
            temperature: 16.,
            error_weight: 128.,
            target_vertex_idx: 0,
            target_edge_idx: 0,
            saved: false,
            mag_state,
            solvers,
            viewer_x_max: 0.,
            viewer_y_max: 0.,
            operation_mode: OperationMode::PointSelect,
            prev_operation_mode: OperationMode::PointSelect,
            view_mode: ViewMode::All,
            range_select_begin_point: Point::new(0.0, 0.0),
            range_select_points: vec![],
            range_select_vertex_indices: vec![],
            range_move_begin_point: Point::new(0.0, 0.0),
            valid_hole_edges: vec![],
            valid_edge_indices: vec![],
            selected_segment: (Point::new(0., 0.), Point::new(0., 0.)),
            segment_move_begin_point: Point::new(0.0, 0.0),
            drag_window_begin_point: Point::new(0.0, 0.0),
            drag_window_begin_mag_state: mag_state,
            mouse_wheel_event_count: 0,
            prev_mouse_wheel_event_count: 0,
            current_bonus: None,
            original_solution: solution,
            best_solution_path,
            panel: ControlPanel {
                solver_mode: SolverMode::Stop,
                temp_slider: Default::default(),
                temp_buttons: DoubleHalveButtonsState {
                    double_button: Default::default(),
                    halve_button: Default::default(),
                },
                error_weight_slider: Default::default(),
                error_buttons: DoubleHalveButtonsState {
                    double_button: Default::default(),
                    halve_button: Default::default(),
                },
            },
            degree_count_by_vertex,
            target_hole_edge: (0, 0),
        })
    }

    fn draw(&mut self, frame: &mut Frame, _timer: &Timer) {
        fn choose_invalid_edge_color(edge_value: &f64, threshold: &f64) -> Color {
            let mut v = if &edge_value.abs() > threshold {
                // 1に近いほど正しい値という設定なので、invalid だったら 0.3 以下にする(r:1, g:<0.3, b:<0.3) という感じ
                1. - (edge_value - threshold) as f32 / 100. - 0.6
            } else {
                1.
            };
            v = if v >= 1. {
                1.
            } else if v <= 0. {
                0.
            } else {
                v
            };
            if edge_value > threshold {
                Color::new(1., v, v, 1.)
            } else {
                Color::new(v, v, 1., 1.)
            }
        }

        frame.clear(Color {
            r: 0.6,
            g: 0.6,
            b: 0.6,
            a: 1.0,
        });

        let mut mesh = Mesh::new();

        let mut hole_points = self
            .problem
            .hole
            .iter()
            .map(|p| to_window_point(p, &self.mag_state))
            .collect_vec();

        hole_points.push(hole_points[0].clone());

        let shape = Shape::Polyline {
            points: hole_points,
        };
        mesh.stroke(shape, Color::BLACK, 2.);

        // 辺表示
        for edge in &self.problem.figure.edges {
            let (idx, jdx) = edge;
            let from = to_window_point(&self.solution.vertices[*idx], &self.mag_state);
            let to = to_window_point(&self.solution.vertices[*jdx], &self.mag_state);
            let shape = Shape::Polyline {
                points: vec![from, to],
            };
            let edge_value = self.solution.edge_value_without_abs(&self.problem, *edge);
            let threshold = self.problem.threshold();

            let edge_color = choose_invalid_edge_color(&edge_value, &threshold);
            mesh.stroke(shape, edge_color, 2.);
        }

        // 頂点表示
        for (idx, vertex) in self.solution.vertices.iter().enumerate() {
            let pos = to_window_point(vertex, &self.mag_state);
            let shape = Shape::Circle {
                center: pos,
                radius: 3.0,
            };

            let visible = match &self.view_mode {
                ViewMode::All => true,
                ViewMode::FilterEdgeDegree { degree } => {
                    self.degree_count_by_vertex[idx] == *degree
                }
            };

            if visible {
                mesh.stroke(shape, Color::RED, 2.0);
            }
        }

        // 操作モードによる表示
        match self.operation_mode {
            OperationMode::PointSelect | OperationMode::PointMove | OperationMode::PointMatch => {
                let pos = to_window_point(
                    &self.solution.vertices[self.target_vertex_idx],
                    &self.mag_state,
                );
                let shape = Shape::Circle {
                    center: pos,
                    radius: 3.,
                };
                mesh.stroke(shape, Color::GREEN, 2.);
            }
            OperationMode::RangeSelect => {
                let shape = Shape::Rectangle(Rectangle {
                    x: self.range_select_begin_point.x,
                    y: self.range_select_begin_point.y,
                    width: self.cursor_position.x - self.range_select_begin_point.x,
                    height: self.cursor_position.y - self.range_select_begin_point.y,
                });
                mesh.stroke(shape, Color::BLACK, 2.);

                let mut points = vec![];
                for &idx in &self.range_select_vertex_indices {
                    let point = to_window_point(&self.solution.vertices[idx], &self.mag_state);
                    points.push(point);
                }
                for point in &points {
                    let shape = Shape::Circle {
                        center: point.clone(),
                        radius: 3.,
                    };
                    mesh.stroke(shape, Color::GREEN, 2.);
                }
            }
            OperationMode::RangeMove => {
                let mut points = vec![];
                for &idx in &self.range_select_vertex_indices {
                    let point = to_window_point(&self.solution.vertices[idx], &self.mag_state);
                    points.push(point);
                }
                for point in &points {
                    let shape = Shape::Circle {
                        center: point.clone(),
                        radius: 3.,
                    };
                    mesh.stroke(shape, Color::GREEN, 2.);
                }
            }
            OperationMode::SegmentSelect
            | OperationMode::SegmentMove
            | OperationMode::SegmentMatch => {
                let edge = self.problem.figure.edges[self.target_edge_idx];
                let from = self.solution.vertices[edge.0];
                let to = self.solution.vertices[edge.1];

                let shape = Shape::Polyline {
                    points: vec![
                        to_window_point(&from, &self.mag_state),
                        to_window_point(&to, &self.mag_state),
                    ],
                };
                mesh.stroke(shape, Color::GREEN, 3.);

                for &(idx, jdx) in &self.valid_hole_edges {
                    let from = to_window_point(&self.problem.hole[idx], &self.mag_state);
                    let to = to_window_point(&self.problem.hole[jdx], &self.mag_state);
                    let shape = Shape::Polyline {
                        points: vec![from, to],
                    };
                    mesh.stroke(shape, Color::GREEN, 3.);
                }
            }
            OperationMode::HoleSegmentSelect => {
                let shape = Shape::Polyline {
                    points: vec![
                        to_window_point(
                            &self.problem.hole[self.target_hole_edge.0],
                            &self.mag_state,
                        ),
                        to_window_point(
                            &self.problem.hole[self.target_hole_edge.1],
                            &self.mag_state,
                        ),
                    ],
                };
                mesh.stroke(shape, Color::GREEN, 3.);

                for &idx in &self.valid_edge_indices {
                    let edge = self.problem.figure.edges[idx];

                    let from = to_window_point(&self.solution.vertices[edge.0], &self.mag_state);
                    let to = to_window_point(&self.solution.vertices[edge.1], &self.mag_state);

                    let shape = Shape::Polyline {
                        points: vec![from, to],
                    };
                    mesh.stroke(shape, Color::GREEN, 3.);
                }
            }
            OperationMode::UseControlPanel => {}
            OperationMode::DragWindow => {}
        }

        let mut vertex_set = HashSet::new();

        for vertex in &self.solution.vertices {
            vertex_set.insert(*vertex);
        }

        // 壁点表示
        for v in self.problem.hole.iter() {
            let pos = to_window_point(v, &self.mag_state);
            let shape = Shape::Circle {
                center: pos,
                radius: 3.0,
            };

            if vertex_set.contains(v) {
                mesh.fill(shape, Color::BLUE);
            } else {
                mesh.stroke(shape, Color::BLUE, 2.0);
            }
        }

        // ボーナス点表示
        for bonus in &self.problem.bonuses {
            let color = match bonus.bonus.as_str() {
                "GLOBALIST" => Color::new(255. / 255., 255. / 255., 0. / 255., 1.),
                "WALLHACK" => Color::new(255. / 255., 165. / 255., 0. / 255., 1.),
                "SUPERFLEX" => Color::new(0. / 255., 255. / 255., 255. / 255., 1.),
                "BREAK_A_LEG" => Color::new(0. / 255., 0. / 255., 255. / 255., 1.),
                &_ => Color::WHITE,
            };
            let pos = to_window_point(&bonus.position, &self.mag_state);
            let shape = Shape::Circle {
                center: pos,
                radius: 3.0,
            };
            mesh.fill(shape, color);
        }

        // 区切り線表示

        let shape = Shape::Polyline {
            points: vec![
                Point::new(self.viewer_x_max, 0.),
                Point::new(self.viewer_x_max, self.viewer_y_max),
            ],
        };
        mesh.stroke(shape, Color::BLACK, 2.);

        let shape = Shape::Polyline {
            points: vec![
                Point::new(0., self.viewer_y_max),
                Point::new(self.viewer_x_max, self.viewer_y_max),
            ],
        };
        mesh.stroke(shape, Color::BLACK, 2.);

        mesh.draw(&mut frame.as_target());
    }

    fn interact(&mut self, input: &mut CustomInput, _window: &mut Window) {
        self.cursor_position = input.cursor_position;
        self.mouse_wheel = input.mouse_wheel;
        self.keys_pressed = input.keys_pressed.clone();
        self.mouse_buttons_pressed = input.mouse_buttons_pressed.clone();

        self.prev_mouse_wheel_event_count = self.mouse_wheel_event_count;
        self.mouse_wheel_event_count = input.mouse_wheel_event_count;
    }

    fn update(&mut self, _window: &Window) {
        match self.panel.solver_mode {
            SolverMode::NakayamaSA => {
                let args = NakayamaSASolverArgument::new(
                    self.temperature as f64,
                    self.error_weight as f64,
                );
                self.solution = self
                    .solvers
                    .nakayama_sa_solver
                    .improve(self.solution.clone(), args);
            }
            SolverMode::GlobalSA => {
                let args =
                    GlobalSASolverArgument::new(self.temperature as f64, self.error_weight as f64);
                self.solution = self
                    .solvers
                    .global_sa_solver
                    .improve(self.solution.clone(), args);
            }
            SolverMode::FixSA => {
                let args =
                    FixSASolverArgument::new(self.temperature as f64, self.error_weight as f64);
                self.solution = self
                    .solvers
                    .fix_sa_solver
                    .improve(self.solution.clone(), args);
            }
            SolverMode::Stop => (),
        }

        // 拡大倍率状態更新
        if self.prev_mouse_wheel_event_count != self.mouse_wheel_event_count {
            if self.mouse_wheel.y >= 0. {
                self.mag_state.mag *= 1.5;
            } else {
                self.mag_state.mag /= 1.5;
            }
        }

        // 区切り位置更新
        let mut max_x = 0;
        let mut max_y = 0;

        for hole_pos in &self.problem.hole {
            max_x = max(max_x, hole_pos.0);
            max_y = max(max_y, hole_pos.1);
        }

        self.viewer_x_max = max_x as f32 * self.mag_state.mag + self.mag_state.x_offset + 50.;
        self.viewer_y_max = max_y as f32 * self.mag_state.mag + self.mag_state.y_offset + 50.;

        // 操作モード更新
        self.current_bonus = None;
        self.prev_operation_mode = self.operation_mode;

        let pressed_ctrl = self.keys_pressed.contains(&keyboard::KeyCode::LControl);
        let pressed_shift = self.keys_pressed.contains(&keyboard::KeyCode::LShift);
        let pressed_left = self.mouse_buttons_pressed.contains(&mouse::Button::Left);
        let pressed_middle = self.mouse_buttons_pressed.contains(&mouse::Button::Middle);
        let pressed_right = self.mouse_buttons_pressed.contains(&mouse::Button::Right);

        if pressed_shift {
            if !pressed_left && !pressed_right {
                self.operation_mode = OperationMode::SegmentSelect;
            }
            if pressed_left && !pressed_right {
                self.operation_mode = OperationMode::SegmentMove;
            }
            if pressed_left && pressed_right {
                self.operation_mode = OperationMode::SegmentMatch;
            }
        } else {
            if !pressed_left && !pressed_right {
                self.operation_mode = OperationMode::PointSelect;
            }
            if pressed_left && !pressed_right {
                self.operation_mode = OperationMode::PointMove;
            }
            if pressed_left
                && pressed_right
                && (self.prev_operation_mode == OperationMode::PointMove
                    || self.prev_operation_mode == OperationMode::PointMatch)
            {
                self.operation_mode = OperationMode::PointMatch;
            }

            let in_viewer = self.cursor_position.y < self.viewer_y_max
                && self.cursor_position.x < self.viewer_x_max;

            if !in_viewer {
                self.operation_mode = OperationMode::UseControlPanel;
            }

            if !pressed_left && pressed_right {
                self.operation_mode = OperationMode::RangeSelect;
            }
            if pressed_left
                && pressed_right
                && (self.prev_operation_mode == OperationMode::RangeSelect
                    || self.prev_operation_mode == OperationMode::RangeMove)
            {
                self.operation_mode = OperationMode::RangeMove;
            }
        }

        if pressed_ctrl {
            self.operation_mode = OperationMode::HoleSegmentSelect;
        }

        if pressed_middle {
            self.operation_mode = OperationMode::DragWindow;
        }

        match self.operation_mode {
            OperationMode::PointSelect => {
                let mut min_dist = f32::MAX;
                for (idx, vertex) in self.solution.vertices.iter().enumerate() {
                    let pos = to_window_point(vertex, &self.mag_state);
                    let dist = (self.cursor_position.x - pos.x).powf(2.)
                        + (self.cursor_position.y - pos.y).powf(2.);
                    if min_dist > dist {
                        min_dist = dist;
                        self.target_vertex_idx = idx;
                    }
                }
            }
            OperationMode::PointMove => {
                let point = to_model_point(&self.cursor_position, &self.mag_state);
                self.solution.vertices[self.target_vertex_idx] = point;
            }
            OperationMode::PointMatch => {
                let mut min_dist = f32::MAX;
                let mut targets = self.problem.hole.clone();

                targets.append(
                    &mut self
                        .problem
                        .bonuses
                        .iter()
                        .map(|b| b.position)
                        .collect_vec(),
                );

                for model_pos in &targets {
                    let pos = to_window_point(model_pos, &self.mag_state);
                    let dist = (self.cursor_position.x - pos.x).powf(2.)
                        + (self.cursor_position.y - pos.y).powf(2.);
                    if min_dist > dist {
                        min_dist = dist;
                        self.solution.vertices[self.target_vertex_idx] = *model_pos;
                        for bonus in &self.problem.bonuses {
                            if bonus.position == *model_pos {
                                self.current_bonus = Some(bonus.clone());
                            }
                        }
                    }
                }
            }
            OperationMode::RangeSelect => {
                if self.prev_operation_mode != self.operation_mode {
                    self.range_select_begin_point = self.cursor_position.clone();
                }
                self.range_select_points.clear();
                self.range_select_vertex_indices.clear();
                for (idx, vertex) in self.solution.vertices.iter().enumerate() {
                    let pos = to_window_point(vertex, &self.mag_state);

                    let (sx, ex) = if self.cursor_position.x < self.range_select_begin_point.x {
                        (self.cursor_position.x, self.range_select_begin_point.x)
                    } else {
                        (self.range_select_begin_point.x, self.cursor_position.x)
                    };

                    let (sy, ey) = if self.cursor_position.y < self.range_select_begin_point.y {
                        (self.cursor_position.y, self.range_select_begin_point.y)
                    } else {
                        (self.range_select_begin_point.y, self.cursor_position.y)
                    };

                    if sx <= pos.x && pos.x <= ex && sy <= pos.y && pos.y <= ey {
                        self.range_select_points.push(pos);
                        self.range_select_vertex_indices.push(idx);
                    }
                }
            }
            OperationMode::RangeMove => {
                if self.prev_operation_mode != self.operation_mode {
                    self.range_move_begin_point = self.cursor_position.clone();
                }
                for (point, idx) in self
                    .range_select_points
                    .iter()
                    .zip(self.range_select_vertex_indices.iter())
                {
                    let dx = self.cursor_position.x - self.range_move_begin_point.x;
                    let dy = self.cursor_position.y - self.range_move_begin_point.y;

                    let point = Point::new(point.x + dx, point.y + dy);
                    let point = to_model_point(&point, &self.mag_state);
                    self.solution.vertices[*idx] = point;
                }
            }
            OperationMode::SegmentSelect => {
                let mut min_dist = f64::MAX;
                for (idx, &edge) in self.problem.figure.edges.iter().enumerate() {
                    let from = to_window_point(&self.solution.vertices[edge.0], &self.mag_state);
                    let to = to_window_point(&self.solution.vertices[edge.1], &self.mag_state);
                    let segment = ((from.x as f64, from.y as f64), (to.x as f64, to.y as f64));
                    let pos = (self.cursor_position.x as f64, self.cursor_position.y as f64);
                    let dist = distance_ps(pos, segment);
                    if min_dist > dist {
                        min_dist = dist;
                        self.target_edge_idx = idx;
                    }
                }
                let (idx, jdx) = self.problem.figure.edges[self.target_edge_idx];

                let point0 = to_window_point(&self.solution.vertices[idx], &self.mag_state);
                let point1 = to_window_point(&self.solution.vertices[jdx], &self.mag_state);

                self.selected_segment = (point0, point1);

                let edge_dist = distance(
                    self.problem.figure.vertices[idx],
                    self.problem.figure.vertices[jdx],
                );

                self.valid_hole_edges.clear();
                for idx in 0..self.problem.hole.len() {
                    let jdx = (idx + 1) % self.problem.hole.len();
                    let hole_edge_dist = distance(self.problem.hole[idx], self.problem.hole[jdx]);
                    if (hole_edge_dist as f64 / edge_dist as f64 - 1.).abs()
                        <= self.problem.threshold()
                    {
                        let edge = (idx, jdx);
                        self.valid_hole_edges.push(edge);
                    }
                }
            }
            OperationMode::SegmentMove | OperationMode::SegmentMatch => {
                if self.prev_operation_mode == OperationMode::SegmentSelect {
                    self.segment_move_begin_point = self.cursor_position.clone();
                }
                let edge = self.problem.figure.edges[self.target_edge_idx];
                let segment = self.selected_segment.clone();

                let v = vec![(edge.0, segment.0), (edge.1, segment.1)];

                for (idx, point) in v {
                    let dx = self.cursor_position.x - self.segment_move_begin_point.x;
                    let dy = self.cursor_position.y - self.segment_move_begin_point.y;

                    let point = Point::new(point.x + dx, point.y + dy);
                    let vertex = to_model_point(&point, &self.mag_state);
                    self.solution.vertices[idx] = vertex;
                }

                if self.operation_mode == OperationMode::SegmentMatch {
                    let selected_segment = (
                        to_window_point(&self.solution.vertices[edge.0], &self.mag_state),
                        to_window_point(&self.solution.vertices[edge.1], &self.mag_state),
                    );
                    let selected_segment = (
                        (selected_segment.0.x as f64, selected_segment.0.y as f64),
                        (selected_segment.1.x as f64, selected_segment.1.y as f64),
                    );

                    for idx in 0..self.problem.hole.len() {
                        let jdx = (idx + 1) % self.problem.hole.len();
                        let hole_segment = (
                            to_window_point(&self.problem.hole[idx], &self.mag_state),
                            to_window_point(&self.problem.hole[jdx], &self.mag_state),
                        );
                        let hole_segment = (
                            (hole_segment.0.x as f64, hole_segment.0.y as f64),
                            (hole_segment.1.x as f64, hole_segment.1.y as f64),
                        );

                        let edge = self.problem.figure.edges[self.target_edge_idx];

                        if intersect_ss(hole_segment, selected_segment) {
                            self.solution.vertices[edge.0] = self.problem.hole[idx];
                            self.solution.vertices[edge.1] = self.problem.hole[jdx];
                        }
                    }
                }
            }
            OperationMode::DragWindow => {
                if self.prev_operation_mode != OperationMode::DragWindow {
                    self.drag_window_begin_point = self.cursor_position.clone();
                    self.drag_window_begin_mag_state = self.mag_state;
                }
                let dx = self.cursor_position.x - self.drag_window_begin_point.x;
                let dy = self.cursor_position.y - self.drag_window_begin_point.y;

                self.mag_state.x_offset = self.drag_window_begin_mag_state.x_offset + dx;
                self.mag_state.y_offset = self.drag_window_begin_mag_state.y_offset + dy;
            }
            OperationMode::HoleSegmentSelect => {
                let mut min_dist = f64::MAX;
                for idx in 0..self.problem.hole.len() {
                    let jdx = (idx + 1) % self.problem.hole.len();
                    let from = to_window_point(&self.problem.hole[idx], &self.mag_state);
                    let to = to_window_point(&self.problem.hole[jdx], &self.mag_state);
                    let s = ((from.x as f64, from.y as f64), (to.x as f64, to.y as f64));
                    let pos = (self.cursor_position.x as f64, self.cursor_position.y as f64);
                    let dist = distance_ps(pos, s);
                    if min_dist > dist {
                        min_dist = dist;
                        self.target_hole_edge = (idx, jdx);
                    }
                }

                self.valid_edge_indices.clear();
                for (idx, &edge) in self.problem.figure.edges.iter().enumerate() {
                    let hole_edge_dist = distance(
                        self.problem.hole[self.target_hole_edge.0],
                        self.problem.hole[self.target_hole_edge.1],
                    );
                    let edge_dist = distance(
                        self.problem.figure.vertices[edge.0],
                        self.problem.figure.vertices[edge.1],
                    );
                    if (hole_edge_dist as f64 / edge_dist as f64 - 1.).abs()
                        <= self.problem.threshold()
                    {
                        self.valid_edge_indices.push(idx);
                    }
                }
            }
            OperationMode::UseControlPanel => {}
        }

        // 描画モード更新

        self.view_mode = ViewMode::All;

        if self.keys_pressed.contains(&keyboard::KeyCode::Key1) {
            self.view_mode = ViewMode::FilterEdgeDegree { degree: 1 }
        } else if self.keys_pressed.contains(&keyboard::KeyCode::Key2) {
            self.view_mode = ViewMode::FilterEdgeDegree { degree: 2 }
        } else if self.keys_pressed.contains(&keyboard::KeyCode::Key3) {
            self.view_mode = ViewMode::FilterEdgeDegree { degree: 3 }
        } else if self.keys_pressed.contains(&keyboard::KeyCode::Key4) {
            self.view_mode = ViewMode::FilterEdgeDegree { degree: 4 }
        } else if self.keys_pressed.contains(&keyboard::KeyCode::Key5) {
            self.view_mode = ViewMode::FilterEdgeDegree { degree: 5 }
        } else if self.keys_pressed.contains(&keyboard::KeyCode::Key6) {
            self.view_mode = ViewMode::FilterEdgeDegree { degree: 6 }
        } else if self.keys_pressed.contains(&keyboard::KeyCode::Key7) {
            self.view_mode = ViewMode::FilterEdgeDegree { degree: 7 }
        } else if self.keys_pressed.contains(&keyboard::KeyCode::Key8) {
            self.view_mode = ViewMode::FilterEdgeDegree { degree: 8 }
        } else if self.keys_pressed.contains(&keyboard::KeyCode::Key9) {
            self.view_mode = ViewMode::FilterEdgeDegree { degree: 9 }
        }

        for bonus in &self.problem.bonuses {
            let current_point = to_model_point(&self.cursor_position, &self.mag_state);
            if (distance(bonus.position, current_point) as f64).sqrt() < 5. {
                self.current_bonus = Some(bonus.clone());
            }
        }

        self.score = self.solution.score_by_ignore_validity(&self.problem);
        self.solution.score = Some(self.score);
        self.valid = self.solution.is_valid(&self.problem);

        if self.keys_pressed.contains(&keyboard::KeyCode::S) {
            if !self.saved {
                // 取り敢えずコンソールに吐き出す
                // TODO ファイルに保存
                println!("{}", self.solution);
                self.saved = true;
            }
        } else {
            self.saved = false;
        }

        // best_score より改善したら出力
        if self.score < self.best_score && self.valid {
            self.best_score = self.score;
            println!("!! ✨✨✨✨✨✨✨✨ update best solution ✨✨✨✨✨✨✨✨ !!");
            println!("{}", self.solution);
            dump_solution(&self.solution, &self.problem).expect("最良解のファイル出力に失敗しました. コンソールに吐き出された json を確認してください");
        }

        if self.keys_pressed.contains(&keyboard::KeyCode::R) {
            self.solution = self.original_solution.clone();
        }

        if self.keys_pressed.contains(&keyboard::KeyCode::B) {
            if let Some(best_solution_path) = &self.best_solution_path {
                self.solution = read_solution(best_solution_path).unwrap();
            }
        }
    }
}

impl UserInterface for PosesPad {
    type Message = Message;
    type Renderer = Renderer;

    fn react(&mut self, msg: Message, _window: &mut Window) {
        match msg {
            Message::TemperatureChanged(temperature) => {
                self.temperature = temperature;
            }
            Message::ErrorWeightChanged(error_weight) => {
                self.error_weight = error_weight;
            }
            Message::SolverModeSelected(mode) => {
                self.panel.solver_mode = mode;
                self.solvers = Solvers::new(&self.problem, &self.solution);
            }
            Message::ButtonClicked(group, kind) => match group {
                ButtonGroup::Temp => match kind {
                    ButtonKind::Double => {
                        self.temperature = f32::min(2. * self.temperature, 10_000.)
                    }
                    ButtonKind::Halve => self.temperature = f32::max(0.5 * self.temperature, 0.001),
                },
                ButtonGroup::Error => match kind {
                    ButtonKind::Double => {
                        self.error_weight =
                            f32::min(f32::max(2. * self.error_weight, 0.001), 1_000.)
                    }
                    ButtonKind::Halve => self.error_weight = f32::max(0.5 * self.error_weight, 0.),
                },
            },
        }
    }

    fn layout(&mut self, window: &Window) -> Element<Message> {
        let mut text = Column::new()
            .max_height(500)
            .spacing(10)
            .push(Text::new(&format!("problem: {}", self.problem.problem)))
            .push(Text::new(&format!(
                "original score: {}",
                self.original_score
            )))
            .push(Text::new(&format!("best score: {}", self.best_score)))
            .push(Text::new(&format!("score: {}", self.score)))
            .push(Text::new(&format!("valid: {}", self.valid)))
            .push(Text::new("-- in cursor --"))
            .push(Text::new(&format!(
                "vertex idx: {}",
                self.target_vertex_idx
            )))
            .push(Text::new(&format!(
                "bonus: {}",
                self.current_bonus
                    .clone()
                    .map(|b| { b.bonus })
                    .unwrap_or("".to_string())
            )))
            .push(Text::new(&format!(
                "bonus target problem: {}",
                self.current_bonus
                    .clone()
                    .map(|b| { format!("{}", b.problem) })
                    .unwrap_or("".to_string())
            )))
            .push(Text::new("-- can use bonuses --"));

        for can_use_bonus in &self.problem.can_use_bonuses {
            let mut used = false;
            if let Some(bonuses) = &self.solution.bonuses {
                for use_bonus in bonuses {
                    if use_bonus == can_use_bonus {
                        used = true;
                    }
                }
            }
            if used {
                text = text.push(
                    Text::new(&format!(
                        "{} from {} (use)",
                        can_use_bonus.bonus, can_use_bonus.problem
                    ))
                    .color(Color::GREEN),
                );
            } else {
                text = text.push(Text::new(&format!(
                    "{} from {}",
                    can_use_bonus.bonus, can_use_bonus.problem
                )));
            }
        }

        let controls = Column::new()
            .max_width(500)
            .spacing(20)
            .push(Row::new().push(double_halve_buttons(
                "Temperature".to_string(),
                &mut self.panel.temp_buttons,
                ButtonGroup::Temp,
                self.temperature,
            )))
            .push(temperature_slider(
                &mut self.panel.temp_slider,
                self.temperature,
            ))
            .push(Row::new().push(double_halve_buttons(
                "Error Weight".to_string(),
                &mut self.panel.error_buttons,
                ButtonGroup::Error,
                self.error_weight,
            )))
            .push(error_weight_slider(
                &mut self.panel.error_weight_slider,
                self.error_weight,
            ))
            .push(mode_selector(self.panel.solver_mode));

        Column::new()
            .width(window.width() as u32)
            .height(window.height() as u32)
            .padding(20)
            .align_items(Align::End)
            .justify_content(Justify::SpaceBetween)
            .push(text)
            .push(controls)
            .into()
    }
}

#[derive(Debug, Clone, Copy)]
pub enum ButtonKind {
    Double,
    Halve,
}

#[derive(Debug, Clone, Copy)]
pub enum ButtonGroup {
    Temp,
    Error,
}

#[derive(Debug, Clone, Copy)]
pub enum Message {
    ButtonClicked(ButtonGroup, ButtonKind),
    TemperatureChanged(f32),
    ErrorWeightChanged(f32),
    SolverModeSelected(SolverMode),
}

fn double_halve_buttons(
    label: String,
    state: &mut DoubleHalveButtonsState,
    group: ButtonGroup,
    value: f32,
) -> Element<Message> {
    Row::new()
        .push(
            Text::new(&format!("{}: {:.*}", label, 7, value))
                .width(450)
                .height(50)
                .horizontal_alignment(HorizontalAlignment::Center)
                .vertical_alignment(VerticalAlignment::Center),
        )
        .push(button_with_label(
            "/2",
            group,
            ButtonKind::Halve,
            &mut state.halve_button,
        ))
        .push(button_with_label(
            "*2",
            group,
            ButtonKind::Double,
            &mut state.double_button,
        ))
        .into()
}

fn temperature_slider(state: &mut slider::State, temperature: f32) -> Element<Message> {
    slider_with_label(
        state,
        0.001..=10_000.0,
        temperature,
        Message::TemperatureChanged,
    )
}

fn error_weight_slider(state: &mut slider::State, error_weight: f32) -> Element<Message> {
    slider_with_label(
        state,
        0.0..=1_000.0,
        error_weight,
        Message::ErrorWeightChanged,
    )
}

fn mode_selector(current: SolverMode) -> Element<'static, Message> {
    let options = [
        SolverMode::Stop,
        SolverMode::NakayamaSA,
        SolverMode::GlobalSA,
        SolverMode::FixSA,
    ]
    .iter()
    .cloned()
    .fold(Row::new().padding(10).spacing(10), |container, mode| {
        container.push(Radio::new(
            mode,
            &format!("{:?}", mode),
            Some(current),
            Message::SolverModeSelected,
        ))
    });

    Column::new()
        .spacing(10)
        .push(Text::new("Solver"))
        .push(options)
        .into()
}

fn button_with_label<'a>(
    label: &str,
    group: ButtonGroup,
    kind: ButtonKind,
    state: &'a mut button::State,
) -> Element<'a, Message> {
    Button::new(state, label)
        .on_press(Message::ButtonClicked(group, kind))
        .into()
}

fn slider_with_label(
    state: &mut slider::State,
    range: RangeInclusive<f32>,
    value: f32,
    on_change: fn(f32) -> Message,
) -> Element<Message> {
    Column::new()
        .spacing(10)
        .push(
            Row::new()
                .spacing(10)
                .push(Slider::new(state, range, value, on_change)),
        )
        .into()
}

fn dump_solution(solution: &Solution, problem: &Problem) -> anyhow::Result<()> {
    let json = serde_json::to_string(solution)?;
    fs::create_dir_all("./solutions")?;
    let score = solution.score(&problem);
    let output_path = format!("./solutions/{}-{}.json", problem.problem, score);
    fs::write(&output_path, json)?;
    println!("Created {}", output_path);
    Ok(())
}

fn to_window_point(p: &omu_solver::model::Point, mag_state: &MagnificationState) -> Point {
    Point::from([
        p.0 as f32 * mag_state.mag + mag_state.x_offset,
        p.1 as f32 * mag_state.mag + mag_state.y_offset,
    ])
}

fn to_model_point(p: &Point, config: &MagnificationState) -> omu_solver::model::Point {
    (
        ((p.x as f32 - config.x_offset) / config.mag) as Coordinate,
        ((p.y as f32 - config.y_offset) / config.mag) as Coordinate,
    )
}
