use anyhow::Result;
use serde_derive::{Deserialize, Serialize};
use std::fs;

#[derive(Serialize, Deserialize)]
pub struct PadConfig {
    pub problem_path: String,
    pub solution_path: Option<String>,
}

impl PadConfig {
    pub fn new(problem_path: String, solution_path: Option<String>) -> Self {
        PadConfig {
            problem_path,
            solution_path,
        }
    }
}

pub fn create_pad_config(config: PadConfig) -> Result<()> {
    let json = serde_json::to_string(&config)?;
    fs::create_dir_all(".pad-tmp")?;
    fs::write(".pad-tmp/pad-config.json", json)?;
    Ok(())
}

pub fn read_pad_config() -> Result<PadConfig> {
    let json = fs::read_to_string(".pad-tmp/pad-config.json")?;
    let config = serde_json::from_str(json.as_str())?;
    Ok(config)
}
