# pad

![](./screenshot.png)
![](./poses-pad.gif)

Solver を実行させながら手動で Poses を操作可能な Pad

## 起動

```bash
$ cargo run -- --problem-path ../problems/1.json
```

## 画面操作

* Solver
  * 実行するソルバーを指定する
* Temperature
  * SA ソルバーに渡す温度
* Error Weight
  * SA ソルバーに渡すエラーに対する重み付け

## キー・マウス操作

* 左クリックで頂点をつかめる
* 左クリック押しっぱなしで頂点移動
* 頂点移動中に右クリックで最寄りの壁頂点に吸い付く
* 右クリックで範囲選択
* 範囲選択中に左クリック押しっぱなしで範囲内の頂点移動
* LShift を押しっぱなしで辺選択モード  
* 辺選択モードで配置できる壁をハイライトする  
* 辺選択モードで右クリックで交差した壁に吸い付く
* LCtrl を押しっぱなしで壁の辺選択モード
* 壁の辺選択モードで配置できる辺をハイライトする
* ホイールで拡大縮小
* ホイールクリック押しっぱなしで表示領域を移動  
* 数字キー入力で対応する次数の頂点のみを表示
* S キーでコンソール solution を出力
* R キーでリセット
* B キーで現状の最良解に変更

## pad の改善結果を提出

```bash
cd ..
cargo run --bin single-submit -- --problem-number 122 --solution-path "pad/solutions/${problem_id}-${score}.json" pad-improve
```