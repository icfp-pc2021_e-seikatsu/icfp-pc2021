use nakayama::{Line, Point};

pub fn is_contains_line_in_polygon(line: &Line, polygon: &[Point]) -> bool {
    let split = 5;
    for k in 1..split {
        let tx = line.a.0 as f64 + (line.b.0 - line.a.0) as f64 / split as f64 * k as f64;
        let ty = line.a.1 as f64 + (line.b.1 - line.a.1) as f64 / split as f64 * k as f64;
        if !contains_point_in_polygon_f64(tx, ty, polygon) {
            return false;
        }
    }

    true
}

pub fn contains_point_in_polygon_f64(tx: f64, ty: f64, polygon: &[Point]) -> bool {
    type Float = f64;
    let mut wn: i32 = 0;

    for idx in 0..polygon.len() {
        let coordinate = &polygon[idx];
        let x0 = coordinate.0 as Float;
        let y0 = coordinate.1 as Float;

        let jdx = (idx + 1) % polygon.len();

        let coordinate = &polygon[jdx];
        let x1 = coordinate.0 as Float;
        let y1 = coordinate.1 as Float;

        if y0 <= ty && y1 > ty {
            let vt = (ty - y0) / (y1 - y0);
            if tx < (x0 + (vt * (x1 - x0))) {
                wn += 1;
            }
        } else if y0 > ty && y1 <= ty {
            let vt = (ty - y0) / (y1 - y0);
            if tx < (x0 + (vt * (x1 - x0))) {
                wn -= 1;
            }
        }
    }

    wn.abs() % 2 == 1
}

pub fn on_the_polygon_edge(point: Point, polygon: &[Point]) -> bool {
    for &p in polygon {
        if p == point {
            return true;
        }
    }

    // polygon の辺上に point が存在しているか
    for idx in 0..polygon.len() {
        let coordinate = &polygon[idx];
        let x0 = coordinate.0;
        let y0 = coordinate.1;

        let jdx = (idx + 1) % polygon.len();

        let coordinate = &polygon[jdx];
        let x1 = coordinate.0;
        let y1 = coordinate.1;

        if x0.min(x1) <= point.0 && point.0 <= x0.max(x1) &&
            y0.min(y1) <= point.1 && point.1 <= y0.max(y1) &&
            (y0 - y1) * (point.0 - x0) == (point.1 - y0) * (x0 - x1) {
            return true;
        }
    }

    false
}
