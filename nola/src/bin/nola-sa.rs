//! 焼なまし
use std::env::args;
use std::fs::File;

use rand::Rng;
use serde::Deserialize;

use nakayama::{contains_point_in_polygon, Ans, Coord, Input, Line, Point};

struct Board {
    epsilon: f64,
    hole_vertices: Vec<Point>,
    hole_lines: Vec<Line>,
    original_dist: Vec<Vec<Coord>>,
}

impl Board {
    fn new(input: &Input) -> Board {
        let mut hole_lines = Vec::with_capacity(input.hole.len());
        for i in 0..input.hole.len() {
            hole_lines.push(Line::new(
                input.hole[i],
                input.hole[(i + 1) % input.hole.len()],
            ));
        }

        let mut original_dist = Vec::with_capacity(input.figure.edges.len());
        for _ in 0..input.figure.edges.len() {
            original_dist.push(Vec::new());
        }
        for &(s, t) in input.figure.edges.iter() {
            let d = input.figure.vertices[s].dist(input.figure.vertices[t]);
            original_dist[s].push(d);
            original_dist[t].push(d);
        }

        Board {
            epsilon: input.epsilon.max(1) as f64 / 1_000_000.0,
            hole_vertices: input.hole.clone(),
            hole_lines,
            original_dist,
        }
    }
}

struct SA {
    edge: Vec<Vec<usize>>,
    vertices: Vec<Point>,
    cost: f64,
    hole_cost: Coord,
    in_the_wall_cost: Coord,
    invalid_edges_cost: Coord,
    best_cost: i64,
    best_vertices: Vec<Point>,
}

impl SA {
    fn new(input: &Input, _init: Point) -> SA {
        let mut edge = Vec::with_capacity(input.figure.vertices.len());
        for _ in 0..input.figure.vertices.len() {
            edge.push(Vec::with_capacity(1));
        }
        for &(s, t) in input.figure.edges.iter() {
            edge[s].push(t);
            edge[t].push(s);
        }
        let a: Ans =
        //     serde_json::from_str(r#"{"vertices":[[17,100],[20,119],[41,108],[30,115],[32,106],[53,94],[39,112],[16,12],[54,103],[56,113],[52,103],[70,45],[35,38],[71,109],[89,94],[80,107],[80,100],[83,69],[187,144],[49,40],[194,185],[189,164],[93,114],[94,92],[99,116],[155,146],[98,79],[96,114],[107,73],[150,166],[65,47],[112,111],[202,158],[238,193],[155,109],[148,106],[133,63],[128,99],[217,180],[131,91],[164,171],[133,75],[162,145],[146,77],[168,161],[158,124],[161,109],[156,118],[160,117],[151,81],[160,144],[166,90],[178,69],[176,139],[184,81],[187,66],[200,83],[181,90],[203,70],[223,73],[236,77],[229,80],[236,59],[260,66],[261,57]]}"#)
            serde_json::from_str(r#"{"vertices":[[171,189],[192,193],[175,170],[210,194],[192,181],[152,163],[228,189],[32,18],[202,160],[220,168],[180,163],[50,29],[58,20],[200,138],[186,112],[162,116],[220,147],[85,31],[143,38],[72,40],[120,63],[124,35],[195,111],[167,98],[177,98],[178,56],[113,42],[217,124],[89,61],[166,38],[61,64],[171,75],[148,28],[143,49],[213,96],[191,80],[91,30],[158,66],[126,34],[136,54],[147,49],[110,42],[173,62],[85,54],[236,90],[167,83],[154,91],[218,71],[119,64],[113,70],[248,68],[128,93],[118,118],[137,41],[99,95],[96,100],[111,66],[110,30],[77,117],[43,116],[90,87],[144,43],[58,91],[13,125],[24,89]]}"#)
                .unwrap();
        SA {
            edge,
            // vertices: vec![init; input.figure.vertices.len()],
            vertices: a.vertices,
            // vertices: input.figure.vertices.clone(),
            cost: 0.0,
            hole_cost: 0,
            in_the_wall_cost: 0,
            invalid_edges_cost: 0,
            best_cost: i64::MAX,
            best_vertices: Vec::new(),
        }
    }

    fn _set_init(&mut self, init: Point) {
        self.vertices = vec![init; self.vertices.len()];
    }

    fn calc_invalid_edges_cost(&self, board: &Board) -> Coord {
        let mut ans = 0;
        for s in 0..self.vertices.len() {
            for &t in self.edge[s].iter() {
                let line = Line::new(self.vertices[s], self.vertices[t]);
                for hole in board.hole_lines.iter() {
                    if line.is_cross(hole) {
                        ans += 1;
                    }
                }
            }
        }
        ans
    }

    fn _is_edge_valid(&self, board: &Board, s: usize, new_point: Point) -> bool {
        if !contains_point_in_polygon(new_point, &board.hole_vertices) {
            return false;
        }
        for &t in self.edge[s].iter() {
            let line = Line::new(new_point, self.vertices[t]);
            for hole in board.hole_lines.iter() {
                if line.is_cross(hole) {
                    return false;
                }
            }
        }
        true
    }

    fn calc_all_error_cost(&self, board: &Board) -> f64 {
        let mut error = 0.0;
        for s in 0..self.vertices.len() {
            error += self.calc_error_cost(board, s, self.vertices[s]);
        }
        error / 2.0
    }

    fn calc_error_cost(&self, board: &Board, s: usize, point: Point) -> f64 {
        let mut error = 0.0;
        for (&t, &od) in self.edge[s].iter().zip(board.original_dist[s].iter()) {
            let d = (1.0 - point.dist(self.vertices[t]) as f64 / od as f64).abs();
            if d > board.epsilon {
                error += d / board.epsilon;
            }
        }
        error
    }

    fn calc_hole_cost(&self, board: &Board) -> Coord {
        let mut cost = 0;
        for &h in board.hole_vertices.iter() {
            let m = self.vertices.iter().map(|v| v.dist(h)).min().unwrap();
            cost += m;
        }
        cost
    }

    fn calc_in_the_wall_cost(&self, board: &Board) -> Coord {
        let mut cost = 0;
        for &h in self.vertices.iter() {
            if contains_point_in_polygon(h, &board.hole_vertices) {
                cost += 1;
            }
        }
        cost
    }

    fn update_all_cost(
        &mut self,
        board: &Board,
        error_weight: f64,
        in_the_wall_weight: f64,
        invalid_edges_weight: f64,
    ) {
        self.hole_cost = self.calc_hole_cost(board);
        self.in_the_wall_cost = self.calc_in_the_wall_cost(board);
        self.invalid_edges_cost = self.calc_invalid_edges_cost(board);
        self.cost = self.calc_all_error_cost(board) * error_weight
            + self.in_the_wall_cost as f64 * in_the_wall_weight
            + self.invalid_edges_cost as f64 * invalid_edges_weight
            + self.hole_cost as f64;
    }

    fn mmc<R: Rng>(
        &mut self,
        rng: &mut R,
        board: &Board,
        beta: f64,
        error_weight: f64,
        in_the_wall_weight: f64,
        invalid_edges_weight: f64,
    ) {
        let v_index = rng.gen_range(0..self.vertices.len());
        let p = self.vertices[v_index];
        let mut q = self.vertices[v_index];
        while q == self.vertices[v_index] {
            q.0 += rng.gen_range(-1..=1);
            q.1 += rng.gen_range(-1..=1);
        }
        // if !self.is_edge_valid(board, v_index, q) {
        //     return;
        // }
        let error_cost_diff = self.calc_error_cost(board, v_index, q)
            - self.calc_error_cost(board, v_index, self.vertices[v_index]);
        self.vertices[v_index] = q;
        let hole_cost_diff = self.calc_hole_cost(board) - self.hole_cost;
        let in_the_wall_cost_diff = self.calc_in_the_wall_cost(board) - self.in_the_wall_cost;
        let invalid_edges_cost_diff = self.calc_invalid_edges_cost(board) - self.invalid_edges_cost;
        let cost_diff = error_cost_diff * error_weight
            + in_the_wall_cost_diff as f64 * in_the_wall_weight
            + invalid_edges_cost_diff as f64 * invalid_edges_weight
            + hole_cost_diff as f64;
        if cost_diff < 0.0 || rng.gen_bool((-cost_diff * beta).exp()) {
            self.hole_cost += hole_cost_diff;
            self.in_the_wall_cost += in_the_wall_cost_diff;
            self.cost += cost_diff;
            let cost = self.cost.round() as i64;
            if cost < self.best_cost
                && self.calc_all_error_cost(board) == 0.0
                && self.calc_in_the_wall_cost(board) == 0
                && self.calc_invalid_edges_cost(board) == 0
            {
                self.best_cost = cost;
                self.best_vertices = self.vertices.clone();
                println!("{}", self.best_cost);
                println!(
                    "{}",
                    serde_json::to_string(&Ans {
                        vertices: self.best_vertices.clone(),
                        bonuses: vec![]
                    })
                    .unwrap()
                );
            }
        } else {
            self.vertices[v_index] = p;
        }
    }
}

#[derive(Debug, Deserialize)]
struct Best {
    problem: String,
    your: i64,
}

fn find_best(problem: &str) -> Best {
    let best_data = include_str!("../../best.txt");
    let mut best = csv::ReaderBuilder::new()
        .delimiter(b'\t')
        .from_reader(best_data.as_bytes());
    for result in best.deserialize() {
        let record: Best = result.unwrap();
        if &record.problem == problem {
            return record;
        }
    }
    Best {
        problem: "".to_owned(),
        your: i64::MAX,
    }
}

fn main() {
    let mut args = args().skip(1);
    let problem = args.next().expect("./bruteforce.rs problem");
    let best = find_best(&problem);
    if best.your == 0 {
        return;
    }

    let input: Input = {
        let path = format!("./problems/{}.json", problem);
        let f = File::open(path).expect("fail to open");
        serde_json::from_reader(f).expect("fail to parse")
    };

    let mut rng = rand_pcg::Mcg128Xsl64::new(999);
    let board = Board::new(&input);
    let mut mc = SA::new(&input, Point(0, 0));
    mc.best_cost = best.your;
    loop {
        // let init = loop {
        //     let x = rng.gen_range(0..100);
        //     let y = rng.gen_range(0..100);
        //     if contains_point_in_polygon(Point(x, y), &board.hole_vertices) {
        //         break Point(x, y);
        //     }
        // };
        // mc.set_init(init);

        let max_step = 1_000_000;
        let t_max: f64 = 100.0;
        let t_min: f64 = 0.01;
        for i in 0..max_step {
            let r = i as f64 / max_step as f64;
            let t = t_max.powf(1.0 - r) * t_min.powf(r);
            let beta = 1.0 / t;
            let error_weight = 150.0;
            let in_the_wall_weight = 500.0;
            let invalid_edges_weight = 500.0;
            if i % 10_000 == 0 {
                eprintln!(
                    "{} {} {} {} {} {}",
                    i,
                    mc.best_cost,
                    mc.calc_all_error_cost(&board),
                    mc.hole_cost,
                    mc.in_the_wall_cost,
                    mc.invalid_edges_cost
                );
            }
            mc.update_all_cost(
                &board,
                error_weight,
                in_the_wall_weight,
                invalid_edges_weight,
            );
            for _ in 0..input.figure.vertices.len() {
                mc.mmc(
                    &mut rng,
                    &board,
                    beta,
                    error_weight,
                    in_the_wall_weight,
                    invalid_edges_weight,
                );
            }
        }
    }
}
