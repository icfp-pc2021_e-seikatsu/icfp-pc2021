//! 焼なまし
use std::env::args;
use std::fs::File;

use rand::Rng;
use serde::Deserialize;

use nakayama::{contains_point_in_polygon, Ans, Coord, Input, Line, Point};
use nola::{is_contains_line_in_polygon, on_the_polygon_edge};

struct Board {
    epsilon: f64,
    hole_vertices: Vec<Point>,
    hole_lines: Vec<Line>,
    original_dist: Vec<Vec<Coord>>,
}

impl Board {
    fn new(input: &Input) -> Board {
        let mut hole_lines = Vec::with_capacity(input.hole.len());
        for i in 0..input.hole.len() {
            hole_lines.push(Line::new(
                input.hole[i],
                input.hole[(i + 1) % input.hole.len()],
            ));
        }

        let mut original_dist = Vec::with_capacity(input.figure.edges.len());
        for _ in 0..input.figure.edges.len() {
            original_dist.push(Vec::new());
        }
        for &(s, t) in input.figure.edges.iter() {
            let d = input.figure.vertices[s].dist(input.figure.vertices[t]);
            original_dist[s].push(d);
            original_dist[t].push(d);
        }

        Board {
            epsilon: input.epsilon.max(1) as f64 / 1_000_000.0,
            hole_vertices: input.hole.clone(),
            hole_lines,
            original_dist,
        }
    }
}

struct SA {
    edge: Vec<Vec<usize>>,
    vertices: Vec<Point>,
    cost: f64,
    hole_cost: Coord,
    best_cost: i64,
    best_vertices: Vec<Point>,
}

impl SA {
    fn new(input: &Input, init: Point) -> SA {
        let mut edge = Vec::with_capacity(input.figure.vertices.len());
        for _ in 0..input.figure.vertices.len() {
            edge.push(Vec::with_capacity(1));
        }
        for &(s, t) in input.figure.edges.iter() {
            edge[s].push(t);
            edge[t].push(s);
        }
        // let a: Ans =
        //     serde_json::from_str(r#"{"vertices":[[98,2],[68,74],[118,1],[15,56],[77,56],[79,37],[84,60],[137,71],[64,68],[63,96],[105,67],[80,78],[108,82],[135,54],[159,26],[60,100],[103,97],[72,103],[107,102],[82,114],[96,110],[125,85],[90,119],[70,124],[181,50],[92,124],[94,141],[88,132],[81,144],[75,130],[142,68],[112,153],[84,163],[118,163],[105,163],[67,187],[118,193],[79,186],[70,187],[48,261],[118,186],[108,202],[118,217],[63,214],[68,223],[62,219],[90,238],[180,254],[118,247],[131,239],[156,245],[73,253],[100,271],[34,339],[252,280],[164,280],[112,308],[260,310]]}"#)
        //         .unwrap();
        SA {
            edge,
            vertices: vec![init; input.figure.vertices.len()],
            // vertices: a.vertices,
            cost: 0.0,
            hole_cost: 0,
            best_cost: i64::MAX,
            best_vertices: Vec::new(),
        }
    }

    fn set_init(&mut self, init: Point) {
        self.vertices = vec![init; self.vertices.len()];
    }

    fn is_edge_valid(&self, board: &Board, s: usize, new_point: Point) -> bool {
        if !contains_point_in_polygon(new_point, &board.hole_vertices)
            && !on_the_polygon_edge(new_point, &board.hole_vertices)
        {
            return false;
        }
        for &t in self.edge[s].iter() {
            let line = Line::new(new_point, self.vertices[t]);
            for hole in board.hole_lines.iter() {
                if line.is_cross(hole) {
                    return false;
                }
                if !is_contains_line_in_polygon(&line, &board.hole_vertices) {
                    return false;
                }
            }
        }
        true
    }

    fn calc_all_error_cost(&self, board: &Board) -> f64 {
        let mut error = 0.0;
        for s in 0..self.vertices.len() {
            error += self.calc_error_cost(board, s, self.vertices[s]);
        }
        error / 2.0
    }

    fn calc_error_cost(&self, board: &Board, s: usize, point: Point) -> f64 {
        let mut error = 0.0;
        for (&t, &od) in self.edge[s].iter().zip(board.original_dist[s].iter()) {
            let d = (1.0 - point.dist(self.vertices[t]) as f64 / od as f64).abs();
            if d > board.epsilon {
                error += d / board.epsilon;
            }
        }
        error
    }

    fn calc_hole_cost(&self, board: &Board) -> Coord {
        let mut cost = 0;
        for &h in board.hole_vertices.iter() {
            let m = self.vertices.iter().map(|v| v.dist(h)).min().unwrap();
            cost += m;
        }
        cost
    }

    fn update_all_cost(&mut self, board: &Board, error_weight: f64) {
        self.hole_cost = self.calc_hole_cost(board);
        self.cost = self.calc_all_error_cost(board) * error_weight + self.hole_cost as f64;
    }

    fn mmc<R: Rng>(&mut self, rng: &mut R, board: &Board, beta: f64, error_weight: f64) {
        let v_index = rng.gen_range(0..self.vertices.len());
        let p = self.vertices[v_index];
        let mut q = self.vertices[v_index];
        while q == self.vertices[v_index] {
            q.0 += rng.gen_range(-1..=1);
            q.1 += rng.gen_range(-1..=1);
        }
        if !self.is_edge_valid(board, v_index, q) {
            return;
        }
        let error_cost_diff = self.calc_error_cost(board, v_index, q)
            - self.calc_error_cost(board, v_index, self.vertices[v_index]);
        self.vertices[v_index] = q;
        let hole_cost_diff = self.calc_hole_cost(board) - self.hole_cost;
        let cost_diff = error_cost_diff * error_weight + hole_cost_diff as f64;
        if cost_diff < 0.0 || rng.gen_bool((-cost_diff * beta).exp()) {
            self.hole_cost += hole_cost_diff;
            self.cost += cost_diff;
            let cost = self.cost.round() as i64;
            if cost < self.best_cost && self.calc_all_error_cost(board) == 0.0 {
                self.best_cost = cost;
                self.best_vertices = self.vertices.clone();
                println!("{}", self.best_cost);
                println!(
                    "{}",
                    serde_json::to_string(&Ans {
                        vertices: self.best_vertices.clone(),
                        bonuses: vec![]
                    })
                    .unwrap()
                );
            }
        } else {
            self.vertices[v_index] = p;
        }
    }
}

#[derive(Debug, Deserialize)]
struct Best {
    problem: String,
    your: Option<i64>,
}

fn find_best(problem: &str) -> Option<i64> {
    let best_data = include_str!("../../best.txt");
    let mut best = csv::ReaderBuilder::new()
        .delimiter(b'\t')
        .from_reader(best_data.as_bytes());
    for result in best.deserialize() {
        let record: Best = result.unwrap();
        if &record.problem == problem {
            return record.your;
        }
    }
    None
}

fn main() {
    let mut args = args().skip(1);
    let problem = args.next().expect("./bruteforce.rs problem");
    let best = find_best(&problem);
    if best == Some(0) {
        return;
    }

    let input: Input = {
        let path = format!("./problems/{}.json", problem);
        let f = File::open(path).expect("fail to open");
        serde_json::from_reader(f).expect("fail to parse")
    };

    let mut rng = rand_pcg::Mcg128Xsl64::new(999);
    let board = Board::new(&input);
    let mut mc = SA::new(&input, Point(0, 0));
    mc.best_cost = best.unwrap_or(i64::MAX);
    loop {
        let init = loop {
            let x = rng.gen_range(0..100);
            let y = rng.gen_range(0..100);
            if contains_point_in_polygon(Point(x, y), &board.hole_vertices)
                || on_the_polygon_edge(Point(x, y), &board.hole_vertices)
            {
                break Point(x, y);
            }
        };
        mc.set_init(init);

        let max_step = 1_000_000;
        let t_max: f64 = rng.gen_range(10.0..10_000.0);
        let t_min: f64 = 0.01;
        let error_weight_min: f64 = rng.gen_range(0.0..100.0);
        let error_weight_max: f64 = rng.gen_range(10.0..1000.0);
        for i in 0..max_step {
            let r = i as f64 / max_step as f64;
            let t = t_max.powf(1.0 - r) * t_min.powf(r);
            let beta = 1.0 / t;
            let error_weight = error_weight_min.powf(1.0 - r) * error_weight_max.powf(r);
            mc.update_all_cost(&board, error_weight);
            for _ in 0..input.figure.vertices.len() {
                mc.mmc(&mut rng, &board, beta, error_weight);
            }
        }
    }
}
