use itertools::Itertools;
use nakayama::{Ans, Input, Line, Point};
use omu_solver::geometry;
use std::env::args;
use std::fs::File;
use std::process::exit;

fn distance(p1: &Point, p2: &Point) -> u64 {
    let ans = (p1.0 - p2.0) * (p1.0 - p2.0) + (p1.1 - p2.1) * (p1.1 - p2.1);
    ans as u64
}

fn dislikes(pose: &Vec<Point>, hole: &Vec<Point>) -> u64 {
    let mut ans = 0;

    for &h in hole.iter() {
        let mut tmp = u64::MAX;
        for &p in pose.iter() {
            tmp = tmp.min(distance(&p, &h));
        }
        ans += tmp;
    }

    ans
}

fn main() {
    let path = args().nth(1).expect("./parallel_move json-path");

    let input: Input = {
        let f = File::open(path.clone()).expect("fail to open");
        serde_json::from_reader(f).expect("fail to parse")
    };

    let mut candidate = Vec::new();

    let polygon = input.hole.clone().iter().map(|x| (x.0, x.1)).collect_vec();

    for tx in 0..=100 {
        for ty in 0..=100 {
            if geometry::contains_point_in_polygon((tx, ty), &polygon) {
                candidate.push(Point(tx, ty))
            }
        }
    }

    if candidate.is_empty() {
        exit(1);
    }

    let mut tmp = vec![Point(-1, -1); input.figure.vertices.len()];

    if let Some(ans) = dfs(&input, &candidate, 0, &mut tmp) {
        println!("{}", serde_json::to_string(&ans).unwrap());
    }
}

fn is_cross_hole(l: &Line, hole: &Vec<Point>) -> bool {
    for i in 0..hole.len() {
        let l2 = Line::new(hole[i].clone(), hole[(i + 1) % hole.len()]);
        if l.is_cross(&l2) {
            return true;
        }
    }

    return false;
}

fn dfs(input: &Input, candidate: &Vec<Point>, idx: usize, tmp: &mut Vec<Point>) -> Option<Ans> {
    let mut ans = None;

    if idx >= tmp.len() {
        return Some(Ans {
            vertices: tmp.clone(),
            bonuses: vec![],
        });
    }

    let mut edges = Vec::new();
    for &e in input.figure.edges.iter() {
        if e.1 == idx {
            edges.push(e.0);
        }
    }
    for &c in candidate.iter() {
        let mut ok = true;
        for &e in edges.iter() {
            let l1 = Line::new(c.clone(), tmp[e].clone());
            let od = input.figure.vertices[idx].dist(input.figure.vertices[e].clone());
            let d = (1.0 - c.dist(tmp[e].clone()) as f64 / od as f64).abs();
            if is_cross_hole(&l1, &input.hole) || d > input.epsilon.max(1) as f64 / 1_000_000.0 {
                ok = false;
                break;
            }
        }
        if ok {
            tmp[idx] = c.clone();
            if let Some(t_ans) = dfs(input, candidate, idx + 1, tmp) {
                if ans.is_none() {
                    ans = Some(t_ans);
                } else if dislikes(&t_ans.vertices, &input.hole)
                    < dislikes(&ans.clone().unwrap().vertices, &input.hole)
                {
                    ans = Some(t_ans);
                }
            }
        }
    }

    return ans;
}
