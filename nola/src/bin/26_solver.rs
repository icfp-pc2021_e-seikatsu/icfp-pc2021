use itertools::Itertools;
use nakayama::{Ans, Input, Point};
use omu_solver::geometry;
use permutohedron::LexicalPermutation;
use std::env::args;
use std::fs::File;
use std::process::exit;

fn distance(p1: &Point, p2: &Point) -> u64 {
    let ans = (p1.0 - p2.0) * (p1.0 - p2.0) + (p1.1 - p2.1) * (p1.1 - p2.1);
    ans as u64
}

fn dislikes(pose: &Vec<Point>, hole: &Vec<Point>) -> u64 {
    let mut ans = 0;

    for &h in hole.iter() {
        let mut tmp = u64::MAX;
        for &p in pose.iter() {
            tmp = tmp.min(distance(&p, &h));
        }
        ans += tmp;
    }

    ans
}

fn main() {
    let path = args().nth(1).expect("./parallel_move json-path");

    let input: Input = {
        let f = File::open(path.clone()).expect("fail to open");
        serde_json::from_reader(f).expect("fail to parse")
    };

    let polygon = input.hole.clone().iter().map(|x| (x.0, x.1)).collect_vec();

    for tx in 0..=80 {
        for ty in 0..=65 {
            if !geometry::contains_point_in_polygon((tx, ty), &polygon) {
                continue;
            }

            let mut per = input.hole.clone();
            per.push(Point(tx, ty));

            per.sort();

            loop {
                let mut ok = true;

                for &(a, b) in input.figure.edges.iter() {
                    let od = input.figure.vertices[a].dist(input.figure.vertices[b].clone());
                    let d = (1.0 - per[a].dist(per[b].clone()) as f64 / od as f64).abs();
                    if d > input.epsilon.max(1) as f64 / 1_000_000.0 {
                        ok = false;
                    }
                }

                if ok && dislikes(&per, &input.hole) == 0 {
                    println!(
                        "{}",
                        serde_json::to_string(&Ans {
                            vertices: per.clone(),
                            bonuses: vec![]
                        })
                        .unwrap()
                    );
                    exit(0);
                }

                if !per.next_permutation() {
                    break;
                }
            }
        }
    }

    exit(0);
}
