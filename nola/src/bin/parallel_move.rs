use itertools::Itertools;
use nakayama::{Ans, Input, Line, Point};
use omu_solver::geometry;
use std::env::args;
use std::fs::File;
use std::process::exit;

fn omu_check(potints: &Vec<Point>, hole: &Vec<Point>) -> bool {
    // hole の中にすべて入ってればOK

    let polygon = hole.clone().iter().map(|x| (x.0, x.1)).collect_vec();
    for p in potints.iter() {
        if !geometry::contains_point_in_polygon((p.0, p.1), &polygon) {
            return false;
        }
    }

    return true;
}

fn distance(p1: &Point, p2: &Point) -> u64 {
    let ans = (p1.0 - p2.0) * (p1.0 - p2.0) + (p1.1 - p2.1) * (p1.1 - p2.1);
    ans as u64
}

fn dislikes(pose: &Vec<Point>, hole: &Vec<Point>) -> u64 {
    let mut ans = 0;

    for &h in hole.iter() {
        let mut tmp = u64::MAX;
        for &p in pose.iter() {
            tmp = tmp.min(distance(&p, &h));
        }
        ans += tmp;
    }

    ans
}

fn main() {
    let path = args().nth(1).expect("./parallel_move json-path");

    let input: Input = {
        let f = File::open(path.clone()).expect("fail to open");
        serde_json::from_reader(f).expect("fail to parse")
    };

    let mut candidate = Vec::new();

    let x_max = input.hole.iter().map(|x| x.0).max().unwrap();
    let y_max = input.hole.iter().map(|x| x.1).max().unwrap();

    for dx in -x_max..=x_max {
        for dy in -y_max..=y_max {
            let tmp_vertices = input
                .figure
                .vertices
                .clone()
                .iter()
                .map(|x| Point(x.0 + dx, x.1 + dy))
                .collect_vec();
            let t_min = *tmp_vertices
                .iter()
                .min_by(|a, b| a.0.min(a.1).cmp(&b.0.min(b.1)))
                .unwrap();
            if t_min.0.min(t_min.1) < 0 {
                continue;
            }
            let mut ok = true;
            for &e in input.figure.edges.iter() {
                let l1 = Line::new(tmp_vertices[e.0].clone(), tmp_vertices[e.1].clone());
                for i in 0..input.hole.len() {
                    let l2 = Line::new(
                        input.hole[i].clone(),
                        input.hole[(i + 1) % input.hole.len()],
                    );
                    if l1.is_cross(&l2) {
                        ok = false;
                        break;
                    }
                }
                if !ok {
                    break;
                }
            }

            if ok {
                if !omu_check(&tmp_vertices, &input.hole) {
                    continue;
                }

                candidate.push((dislikes(&tmp_vertices, &input.hole), tmp_vertices.clone()));
            }
        }
    }

    if candidate.is_empty() {
        exit(1);
    }

    let ans = candidate.into_iter().min_by(|a, b| a.0.cmp(&b.0)).unwrap();
    let ans = ans.clone();

    println!(
        "{}",
        serde_json::to_string(&Ans {
            vertices: ans.1,
            bonuses: vec![]
        })
        .unwrap()
    );
    // println!("{} {}", path, ans.0);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_data_parse() {
        let data = include_str!("../../../problems/1.json");
        let input: Input = serde_json::from_reader(data.as_bytes()).unwrap();
        assert_eq!(input.epsilon, 150000);
    }
}
