import json
import math
import glob

score = 0
for p in glob.glob(f'problems/*.json'):
    with open(p) as f:
        d = json.load(f)
        s = math.ceil(
            (math.log2(len(d['hole'])
                       * len(d['figure']['edges'])
                       * len(d['figure']['vertices']) / 6)) * 1000)
        score += s
        print(f'{p}: {s}')

print(score)
