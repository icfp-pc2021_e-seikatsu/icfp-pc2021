import { IApiService } from './IService';
import { ApiService } from './Service';

export type Services = {
  readonly apiService: IApiService;
};

export const setupDependencies: () => Promise<Services> = async () => {
  const ApiServiceInst = new ApiService();
  return {
    apiService: ApiServiceInst
  };
};
