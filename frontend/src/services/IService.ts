import { SubmitResult, ScoreResult, Solution } from '../models/Model';

export interface IApiService {
  getSubmits(num: number): Promise<SubmitResult>;
  getScore(num: number, body: Solution): Promise<ScoreResult>;
}
