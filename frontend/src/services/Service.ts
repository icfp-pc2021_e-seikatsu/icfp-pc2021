import { ApiHost } from "../consts/Application";
import { SubmitResult, ScoreResult, Solution } from "../models/Model";

import { IApiService } from "./IService";

export class ApiService implements IApiService {
  async getSubmits(num: number): Promise<SubmitResult> {
    const url = ApiHost + "/problems/" + num;
    const resp = await fetch(new URL(url, ApiHost).toString());
    return await resp.json();
  }
  async getScore(num: number, body: Solution): Promise<ScoreResult> {
    const url = ApiHost + "/problems/" + num + "/solutions";
    const resp = await fetch(new URL(url, ApiHost).toString(), {
      method: "POST",
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(body),
    });
    return await resp.json();
  }
}
