import { Box, Button, TextField, Typography } from "@material-ui/core";

import { useDependency } from "../hooks/DependencyHook";
import { ProblemState } from "../models/Model";

interface EditProps {
  problemState: ProblemState;
  setProblemState: React.Dispatch<React.SetStateAction<ProblemState>>;
}

const EditForm: React.FC<EditProps> = ({ problemState, setProblemState }) => {
  const apiService = useDependency("apiService");

  const getScore = () => {
    async function load(): Promise<void> {
      const res = await apiService.getScore(problemState.problem_id, {
        vertices: problemState.problem.figure.vertices,
      });
      setProblemState({
        ...problemState,
        score: res.score,
      });
    }
    load();
  };
  const handleChange = (value: string) => {
    setProblemState({
      ...problemState,
      problem: {
        ...problemState.problem,
        figure: {
          ...problemState.problem.figure,
          vertices: JSON.parse(value).vertices,
        },
      },
    });
  };

  return (
    <Box>
      <Typography>score: {problemState.score}</Typography>
      <TextField
        disabled
        multiline
        fullWidth
        value={JSON.stringify({
          bonuses: problemState.problem.bonuses,
        })}
      />
      <TextField
        disabled
        multiline
        fullWidth
        value={JSON.stringify({
          hole: problemState.problem.hole,
        })}
      />
      <TextField
        multiline
        fullWidth
        onChange={(event) => handleChange(event.target.value)}
        value={JSON.stringify({
          vertices: problemState.problem.figure.vertices,
        })}
      />
      <Button variant="contained" color="secondary" onClick={() => getScore()}>
        スコア計算
      </Button>
    </Box>
  );
};

export default EditForm;
