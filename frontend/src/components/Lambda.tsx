import { Problem } from "../models/Model";

type LambdaProps = {
  problem: Problem;
};

const hsv2rgb = (h: number, s: number) => {
  let v = 90;

  const rgb = { r: 0, g: 0, b: 0 };

  if (h == 360) {
    h = 0;
  }

  s = s / 100;
  v = v / 100;

  if (s == 0) {
    rgb.r = v * 255;
    rgb.g = v * 255;
    rgb.b = v * 255;
    return (
      "#" +
      ("0" + rgb.r.toString(16)).slice(-2) +
      ("0" + rgb.g.toString(16)).slice(-2) +
      ("0" + rgb.b.toString(16)).slice(-2)
    );
  }

  const dh = Math.floor(h / 60);
  const p = v * (1 - s);
  const q = v * (1 - s * (h / 60 - dh));
  const t = v * (1 - s * (1 - (h / 60 - dh)));

  switch (dh) {
    case 0:
      rgb.r = v;
      rgb.g = t;
      rgb.b = p;
      break;
    case 1:
      rgb.r = q;
      rgb.g = v;
      rgb.b = p;
      break;
    case 2:
      rgb.r = p;
      rgb.g = v;
      rgb.b = t;
      break;
    case 3:
      rgb.r = p;
      rgb.g = q;
      rgb.b = v;
      break;
    case 4:
      rgb.r = t;
      rgb.g = p;
      rgb.b = v;
      break;
    case 5:
      rgb.r = v;
      rgb.g = p;
      rgb.b = q;
      break;
  }

  rgb.r = Math.round(rgb.r * 255);
  rgb.g = Math.round(rgb.g * 255);
  rgb.b = Math.round(rgb.b * 255);

  return (
    "#" +
    ("0" + rgb.r.toString(16)).slice(-2) +
    ("0" + rgb.g.toString(16)).slice(-2) +
    ("0" + rgb.b.toString(16)).slice(-2)
  );
};

const Lambda = (props: LambdaProps) => {
  const problem = props.problem;
  const points = [];

  const hole_array = [];
  for (let i = 0; i < problem.hole.length; i++) {
    if (i == 0) {
      hole_array.push(`M ${problem.hole[i][0]},${problem.hole[i][1]}`);
    } else {
      hole_array.push(`L ${problem.hole[i][0]},${problem.hole[i][1]}`);
    }
    points.push(problem.hole[i][0]);
    points.push(problem.hole[i][1]);
  }

  for (let i = 0; i < problem.figure.vertices.length; i++) {
    points.push(problem.figure.vertices[i][0]);
    points.push(problem.figure.vertices[i][1]);
  }

  hole_array.push(
    `Z M 0,0 L ${Math.max(...points) + 5},0 L ${Math.max(...points) + 5},${
      Math.max(...points) + 5
    } L 0,${Math.max(...points) + 5}`
  );
  const size = `0 0 ${Math.max(...points) + 5} ${Math.max(...points) + 5}`;

  const fig_array = [];
  for (let i = 0; i < problem.figure.edges.length; i++) {
    const edge = problem.figure.edges[i];
    fig_array.push(
      `M ${problem.figure.vertices[edge[0]][0]},${
        problem.figure.vertices[edge[0]][1]
      } L ${problem.figure.vertices[edge[1]][0]},${
        problem.figure.vertices[edge[1]][1]
      }`
    );
  }

  const color_map = {
    GLOBALIST: "yellow",
    WALLHACK: "orange",
    BREAK_A_LEG: "blue",
    SUPERFLEX: "cyan",
  } as {[key: string]: string}

  const bonus_array = [];
  for (let i = 0; i < problem.bonuses.length; i++) {
    const bonus = problem.bonuses[i];
    bonus_array.push({
      x: bonus.position[0],
      y: bonus.position[1],
      color: color_map[bonus.bonus],
    });
  }

  return (
    <svg viewBox={size}>
      <g id="bonus" transform="translate(0.0, 0.0)">
        {bonus_array.map((row, idx) => (
          <circle cx={row.x} cy={row.y} r="5.0" style={{ fill: row.color }} />
        ))}
      </g>
      <path
        d={hole_array.join(" ")}
        id="hole"
        style={{ fill: "#00000066", fillRule: "evenodd", stroke: "none" }}
      />
      <g
        id="figure"
        style={{ fill: "none", stroke: "#ff0000", strokeLinecap: "round" }}
        transform="translate(0.0, 0.0)"
      >
        {fig_array.map((row, idx) => (
          <path d={row} stroke={hsv2rgb(idx * 3, 100 - idx * 3)} />
        ))}
      </g>
    </svg>
  );
};

export default Lambda;
