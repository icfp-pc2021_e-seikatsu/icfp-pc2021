import {
  AppBar,
  Toolbar,
  Typography,
  Tabs,
  Tab,
  Link,
} from "@material-ui/core";
import InsertEmoticonIcon from "@material-ui/icons/InsertEmoticon";
import React from "react";

const Header = () => {
  return (
    <AppBar position="static">
      <Toolbar>
        <InsertEmoticonIcon />
        <Typography>chirijako</Typography>
        <InsertEmoticonIcon />
        <Tabs>
        <Link href="/" color="inherit">
            <Tab label="Home" />
          </Link>
          <Link href="https://poses.live/teams" color="inherit">
            <Tab label="Portal" />
          </Link>
          <Link href="/tic" color="inherit">
            <Tab label="Tic" />
          </Link>
        </Tabs>
      </Toolbar>
    </AppBar>
  );
};

export default Header;
