import { Table, Button, Box, FormControl, InputLabel, Input, TableRow, TableHead, TableBody, TableCell } from "@material-ui/core";

import { Figure, Point } from "../models/Model";

interface LengthProps {
  figure: Figure;
};

const len = (p0: Point, p1: Point) => (p0[0] - p1[0]) ** 2 + (p0[1] - p1[1]) ** 2;


const Length: React.FC<LengthProps> = ({ figure }) => {
  const edges = figure.edges;
  const vertices = figure.vertices;
  const edgeRows = edges.map(r => {
    return (
      <TableRow>
        <TableCell>{r[0]}</TableCell>
        <TableCell>{r[1]}</TableCell>
      </TableRow>
    )
  })

  return (

    <Table>
      <TableHead>
        <TableRow>
          <TableCell>vertex-1 idx</TableCell>
          <TableCell>vertex-2 idx</TableCell>
          <TableCell>distance</TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        {edges.map(r => {
          return (
            <TableRow>
              <TableCell width={10}>{r[0]}</TableCell>
              <TableCell width={10}>{r[1]}</TableCell>
              <TableCell>{len(vertices[r[0]], vertices[r[1]])}</TableCell>
            </TableRow>
          )
        })}
      </TableBody>
    </Table>
  );
}

export default Length;