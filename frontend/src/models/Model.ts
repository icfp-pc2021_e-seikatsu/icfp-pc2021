import { Repeat } from 'typescript-tuple';

export type Point = Repeat<number, 2>;

export interface Bonus {
    readonly bonus: string,
    readonly problem: number,
    readonly position: Point,
}

export interface UseBonus {
    readonly bonus: string,
    readonly problem: number,
}

export interface Problem {
    readonly bonuses: Bonus[],
    readonly can_use_bonuses: UseBonus[],
    readonly hole: Point[],
    readonly epsilon: number,
    readonly figure: Figure,
}

export interface Figure {
    readonly edges: Point[],
    readonly vertices: Point[],
}

export interface Submit {
    readonly vertices: Point[],
    readonly score: number,
    readonly name: string,
}

export interface SubmitResult {
    readonly problem_id: number,
    readonly problem: Problem,
    readonly submits: Submit[],
    readonly minimal: number,
}

export interface ScoreResult {
    readonly score: number,
}

export interface Solution {
    readonly vertices: Point[],
}

export type ProblemState = {
    problem: Problem,
    score: number,
    problem_id: number,
};