import {
  Link,
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  Typography,
} from "@material-ui/core";
import React, { useState, useEffect } from "react";

import Lambda from "../components/Lambda";
import { useDependency } from "../hooks/DependencyHook";
import { SubmitResult } from "../models/Model";

type DashboardState = {
  submitResults: SubmitResult[];
};

const Dashboard = () => {
  const [DashboardState, setDashboardState] = useState<DashboardState>({
    submitResults: [],
  });
  const apiService = useDependency("apiService");

  useEffect(() => {
    async function load(): Promise<void> {
      for (let i = 1; i <= 132; i++) {
        const res = await apiService.getSubmits(i);
        const ss = DashboardState.submitResults;
        ss.push(res);
        setDashboardState({ submitResults: ss });
      }
    }
    load();
  }, [apiService]);

  return (
    <Table stickyHeader>
      <colgroup>
        <col style={{ width: "4%" }} />
        <col style={{ width: "12%" }} />
        <col style={{ width: "12%" }} />
        <col style={{ width: "12%" }} />
        <col style={{ width: "12%" }} />
        <col style={{ width: "12%" }} />
        <col style={{ width: "12%" }} />
        <col style={{ width: "12%" }} />
        <col style={{ width: "12%" }} />
      </colgroup>
      <TableHead>
        <TableRow style={{ backgroundColor: "#f5f5f5", height: "35px" }}>
          <TableCell align="center">
            <Typography>ID</Typography>
          </TableCell>
          <TableCell align="center">
            <Typography>info</Typography>
          </TableCell>
          <TableCell align="center">
            <Typography>original</Typography>
          </TableCell>
          <TableCell align="center">
            <Typography>1</Typography>
          </TableCell>
          <TableCell align="center">
            <Typography>2</Typography>
          </TableCell>
          <TableCell align="center">
            <Typography>3</Typography>
          </TableCell>
          <TableCell align="center">
            <Typography>4</Typography>
          </TableCell>
          <TableCell align="center">
            <Typography>5</Typography>
          </TableCell>
          <TableCell align="center">
            <Typography>6</Typography>
          </TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        {DashboardState.submitResults.map((row, idx) => {
          return (
            <TableRow
              style={{ backgroundColor: "#f5f5f5", height: "35px" }}
              hover
              key={`table-row-row-${idx}`}
            >
              <TableCell style={{ height: "auto !important" }}>
                <Link href={`https://poses.live/problems/${row.problem_id}`}>
                  <Typography>{row.problem_id}</Typography>
                </Link>
              </TableCell>
              <TableCell style={{ height: "auto !important" }}>
                <Typography color="error">
                  score:{" "}
                  {row.submits.length > 0
                    ? Math.ceil(
                        1000 *
                          Math.log2(
                            (row.problem.hole.length *
                              row.problem.figure.edges.length *
                              row.problem.figure.vertices.length) /
                              6
                          ) *
                          Math.sqrt(
                            (row.minimal + 1) / (row.submits[0].score + 1)
                          )
                      )
                    : null}
                </Typography>
                <Typography>
                  factor:{" "}
                  {Math.ceil(
                    1000 *
                      Math.log2(
                        (row.problem.hole.length *
                          row.problem.figure.edges.length *
                          row.problem.figure.vertices.length) /
                          6
                      )
                  )}
                </Typography>
                <Typography>minimal: {row.minimal}</Typography>
                {row.problem.bonuses.map((b, i) => (
                  <Typography>
                    {b.bonus}: {b.problem}
                  </Typography>
                ))}
              </TableCell>
              <TableCell style={{ height: "auto !important" }}>
                <Link href={`/detail/${row.problem_id}/0`}>
                  <Lambda problem={row.problem} />
                </Link>
                <Typography>epsilon: {row.problem.epsilon}</Typography>
              </TableCell>

              {row.submits.slice(0, 6).map((submit, i) => {
                let color = "default";

                const color_map = {
                  GLOBALIST: "#ffec50",
                  WALLHACK: "#fbb034",
                  BREAK_A_LEG: "#a5befa",
                  SUPERFLEX: "#00E4FF",
                } as { [key: string]: string };

                for (const b of row.problem.bonuses) {
                  for (const s of submit.vertices) {
                    if (b.position[0] == s[0] && b.position[1] == s[1]) {
                      color = color_map[b.bonus];
                    }
                  }
                }

                return (
                  <TableCell
                    style={{
                      height: "auto !important",
                      backgroundColor: color,
                    }}
                  >
                    <Link href={`/detail/${row.problem_id}/${i + 1}`}>
                      <Lambda
                        problem={{
                          ...row.problem,
                          figure: {
                            ...row.problem.figure,
                            vertices: submit.vertices,
                          },
                        }}
                      />
                    </Link>
                    <Typography>
                      {submit.name.slice(0, 11)}: {submit.score}
                    </Typography>
                  </TableCell>
                );
              })}
            </TableRow>
          );
        })}
      </TableBody>
    </Table>
  );
};

export default Dashboard;
