import { Box, Button, Grid } from "@material-ui/core";
import {
  ArrowBackIosRounded,
  ArrowForwardIosRounded,
} from "@material-ui/icons";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import ArrowDownwardIcon from "@material-ui/icons/ArrowDownward";
import ArrowForwardIcon from "@material-ui/icons/ArrowForward";
import ArrowUpwardIcon from "@material-ui/icons/ArrowUpward";
import { useState, useEffect } from "react";
import { useParams } from "react-router";

import EditForm from "../components/EditForm";
import Lambda from "../components/Lambda";
import Length from "../components/Length";
import { useDependency } from "../hooks/DependencyHook";
import { ProblemState } from "../models/Model";

export type DetailQuery = {
  problemId: number;
  solutionId: number;
};

const Detail = () => {
  const params = useParams() as unknown as DetailQuery;
  const [problemState, setProblemState] = useState<ProblemState>({
    problem: {
      can_use_bonuses: [],
      bonuses: [],
      hole: [],
      epsilon: 0,
      figure: {
        edges: [],
        vertices: [],
      },
    },
    score: -1,
    problem_id: params.problemId,
  });
  const apiService = useDependency("apiService");

  useEffect(() => {
    async function load(): Promise<void> {
      const submit_res = await apiService.getSubmits(params.problemId);
      if (params.solutionId == 0) {
        setProblemState({
          ...problemState,
           problem: submit_res.problem,
         });
      } else {
        setProblemState({
          ...problemState,
          problem: {
            ...submit_res.problem,
            figure: {
              ...submit_res.problem.figure,
              vertices: submit_res.submits[params.solutionId - 1].vertices,
            },
          },
          score: submit_res.submits[params.solutionId - 1].score,
        });
      }
    }
    load();
  }, [apiService]);

  const pararell = (direction: number, val: number) => {
    const v = problemState.problem.figure.vertices.slice();
    for (let i = 0; i < v.length; i++) {
      v[i][direction] += val;
    }
    setProblemState({
      ...problemState,
      problem: {
        ...problemState.problem,
        figure: {
          ...problemState.problem.figure,
          vertices: v,
        },
      },
    });
  };

  const rotate = (val: number) => {
    const cos = Math.cos(val);
    const sin = Math.sin(val);
    const v = problemState.problem.figure.vertices.slice();
    let gx = 0;
    let gy = 0;
    for (let i = 0; i < v.length; i++) {
      const [x, y] = v[i];
      gx += x;
      gy += y;
    }
    gx /= v.length;
    gy /= v.length;
    for (let i = 0; i < v.length; i++) {
      let [x, y] = v[i];
      x -= gx;
      y -= gy;
      v[i][0] = x * cos - y * sin + gx;
      v[i][1] = x * sin + y * cos + gx;
    }
    setProblemState({
      ...problemState,
      problem: {
        ...problemState.problem,
        figure: {
          ...problemState.problem.figure,
          vertices: v,
        },
      },
    });
  };

  return (
    <Grid container spacing={1}>
      <Grid item xs={4}>
        <Box m={1}>
          <Lambda problem={problemState.problem} />
        </Box>
      </Grid>
      <Grid item xs={8}>
        <Box margin={1}>
          <Button
            variant="contained"
            color="secondary"
            onClick={() => pararell(0, -10)}
          >
            <ArrowBackIcon />
          </Button>
          <Button
            variant="contained"
            color="secondary"
            onClick={() => pararell(1, -10)}
          >
            <ArrowUpwardIcon />
          </Button>
          <Button
            variant="contained"
            color="secondary"
            onClick={() => pararell(1, 10)}
          >
            <ArrowDownwardIcon />
          </Button>
          <Button
            variant="contained"
            color="secondary"
            onClick={() => pararell(0, 10)}
          >
            <ArrowForwardIcon />
          </Button>
          <Button
            variant="contained"
            color="secondary"
            onClick={() => rotate(0.10)}
          >
            <ArrowBackIosRounded />
          </Button>
          <Button
            variant="contained"
            color="secondary"
            onClick={() => rotate(-0.1)}
          >
            <ArrowForwardIosRounded />
          </Button>
        </Box>
        <Grid container spacing={1}>
          <Grid item xs={8}>
            <EditForm
              problemState={problemState}
              setProblemState={setProblemState}
            />
          </Grid>
        </Grid>
      </Grid>
      <Grid item xs={4}>
        <Grid container spacing={1}>
          <Length figure={problemState.problem.figure} />
        </Grid>
      </Grid>
    </Grid>
  );
};

export default Detail;
