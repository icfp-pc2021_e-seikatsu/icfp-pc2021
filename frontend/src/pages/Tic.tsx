import { Container, Grid, Box, Button, Typography, List, ListItem } from '@material-ui/core';
import React, { useState } from 'react';
import { Repeat } from 'typescript-tuple';

type SquareState = 'O' | 'X' | null;

type SquareProps = {
  value: SquareState;
  onClick: () => void;
};

const Square = (props: SquareProps) => (
  <Button variant="contained" onClick={props.onClick} style={{ height: 40 }}>
    {props.value}
  </Button>
);

type BoardState = Repeat<SquareState, 9>;

type BoardProps = {
  squares: BoardState;
  onClick: (i: number) => void;
};

const Board = (props: BoardProps) => {
  const renderSquare = (i: number) => (
    <Square value={props.squares[i]} onClick={() => props.onClick(i)} />
  );

  return (
    <Box margin={2}>
      <Grid item>
        {renderSquare(0)}
        {renderSquare(1)}
        {renderSquare(2)}
      </Grid>
      <Grid item>
        {renderSquare(3)}
        {renderSquare(4)}
        {renderSquare(5)}
      </Grid>
      <Grid item>
        {renderSquare(6)}
        {renderSquare(7)}
        {renderSquare(8)}
      </Grid>
    </Box>
  );
};

const calculateWinner = (squares: BoardState) => {
  const lines = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
  ];
  for (const line of lines) {
    const [a, b, c] = line;
    if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
      return squares[a];
    }
  }
  return null;
};

type Step = {
  squares: BoardState;
  xIsNext: boolean;
};

type GameState = {
  readonly history: Step[];
  readonly stepNumber: number;
};

const Tic = () => {
  const [state, setState] = useState<GameState>({
    history: [
      {
        squares: [null, null, null, null, null, null, null, null, null],
        xIsNext: true,
      },
    ],
    stepNumber: 0,
  });

  const current = state.history[state.stepNumber];
  const winner = calculateWinner(current.squares);
  let status: string;
  if (winner) {
    status = `Winner: ${winner}`;
  } else {
    status = `Next player: ${current.xIsNext ? 'X' : 'O'}`;
  }

  const handleClick = (i: number) => {
    if (winner || current.squares[i]) {
      return;
    }

    const next: Step = (({ squares, xIsNext }) => {
      const nextSquares = squares.slice() as BoardState;
      nextSquares[i] = xIsNext ? 'X' : 'O';
      return {
        squares: nextSquares,
        xIsNext: !xIsNext,
      };
    })(current);

    setState(({ history, stepNumber }) => {
      const newHistory = history.slice(0, stepNumber + 1).concat(next);

      return {
        history: newHistory,
        stepNumber: newHistory.length - 1,
      };
    });
  };

  const jumpTo = (move: number) => {
    setState((prev) => ({
      ...prev,
      stepNumber: move,
    }));
  };

  const moves = state.history.map((step, move) => {
    const desc = move > 0 ? `Go to move #${move}` : 'Go to game start';
    return (
      <ListItem>
        <Button variant="contained" color="secondary" onClick={() => jumpTo(move)}>{desc}</Button>
      </ListItem>
    );
  });

  return (
    <Container>
      <Grid container>
        <Grid item>
          <Board squares={current.squares} onClick={handleClick} />
        </Grid>
        <Grid item>
          <Box margin={2}>
            <Typography>{status}</Typography>
            <List>{moves}</List>
          </Box>
        </Grid>
      </Grid>
    </Container>
  );
};

export default Tic;
