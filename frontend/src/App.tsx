import CssBaseline from "@material-ui/core/CssBaseline";
import blueGrey from "@material-ui/core/colors/blueGrey";
import lime from "@material-ui/core/colors/lime";
import { ThemeProvider } from "@material-ui/core/styles";
import { createMuiTheme } from "@material-ui/core/styles";
import React, { useState, useEffect, StrictMode, Suspense, lazy } from "react";
import ReactDOM from "react-dom";
import { BrowserRouter, Route, Switch } from "react-router-dom";

import Header from "./components/Header";
import { DependencyProvider } from "./hooks/DependencyHook";
import type { Services } from "./services/SetupDependencies";

const Dashboard = lazy(() => import("./pages/Dashboard"));
const Detail = lazy(() => import("./pages/Detail"));
const Tic = lazy(() => import("./pages/Tic"));

const theme = createMuiTheme({
  palette: {
    primary: {
      main: blueGrey[500],
    },
    secondary: {
      main: lime[200],
    },
  },
});

const App = () => {
  const [services, setServices] = useState<Services | null>(null);

  useEffect(() => {
    import("./services/SetupDependencies")
      .then((module) => module.setupDependencies())
      .then((services) => {
        setServices(services);
      });
  }, []);

  return (
    <>
      {services !== null ? (
        <DependencyProvider services={services}>
          <ThemeProvider theme={theme}>
            <CssBaseline />
            <Header />
            <Switch>
              <Route exact path="/" component={Dashboard} />
              <Route exact path="/tic" component={Tic} />
              <Route exact path="/detail/:problemId/:solutionId" component={Detail} />
            </Switch>
          </ThemeProvider>
        </DependencyProvider>
      ) : (
        <span>Loading...</span>
      )}
    </>
  );
};

export default App;