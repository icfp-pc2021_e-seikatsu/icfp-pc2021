# icfp-pc2021

* team: chirijako
* member:
    * @j_234ko
    * @nola_suz
    * @omuric
    * @hinohi (@daiju.nakayama)
    * @akagenorobin

## About our works

Two major things.
* The first is to keep a simulated annealing solver running on a large computer (96vCPU).
* The second is to create a GUI and optimize the pose by hand.
  We make the GUI that solver can optimize in real time, work with human in harmony.

## official

* https://icfpcontest2021.github.io/
* https://poses.live/problems

## secret

```
chirijako
password: V8EFsAsiawPy64a
```

```
api_key: 7ec48e69-ddb3-43d3-95c5-54396b198224
```

## Tools & Solvers

### GUI (Pad)

Rust.

show [pad](./pad/README.md)

### Web Dashboard

frontend (TypeScript+React)

```
cd frontend
npm ci
npm start
```

backend (Rust+actix-web)

```
cd problem-api
cargo run
```

### Simulated annealing solver

Rust.

places at nakayama.

1. The initial state generates a state where all the points are gathered in one place.
1. Only transitions that move a single point

### Specify solvers

Rust.

* nora: written by @nola_suz
* omu: written by @omuric
* sugihara: @akagenorobin

### Others

* ranking_bot: Scraping the ranking and notifying slack
* submit-tool: just like its name
