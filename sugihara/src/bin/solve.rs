use omu_solver::model::{Problem, Solution};
use omu_solver::solver::Solver;
//use omu_solver::solver::{
//    sa::{NakayamaSASolver, NakayamaSASolverArgument},
//};
use std::fs::File;
use structopt::StructOpt;
use sugihara::global::{GlobalSASolver, GlobalSASolverArgument};

#[derive(Debug, StructOpt)]
struct Opt {
    problem_number: u32,
    #[structopt(long)]
    init_path: String,
}

fn main() {
    let opt: Opt = Opt::from_args();

    let problem: Problem = {
        let path = format!("./problems/{}.json", opt.problem_number);
        let f = File::open(path).expect("fail to open");
        serde_json::from_reader(f).expect("fail to parse")
    };

    let f = File::open(opt.init_path).expect("fail to open");
    let mut solution: Solution = serde_json::from_reader(f).expect("fail to parse");

    let mut solver = GlobalSASolver::new(&problem, &solution);
    //let mut solver = NakayamaSASolver::new(&problem, &solution);
    let max_step = 1_000_000;
    let t_max: f64 = 200.0;
    let t_min: f64 = 0.01;
    let error_weight_min: f64 = 50.0;
    let error_weight_max: f64 = 200.0;
    for i in 0..max_step {
        let r = i as f64 / max_step as f64;
        let t = t_max.powf(1.0 - r) * t_min.powf(r);
        let error_weight = error_weight_min.powf(1.0 - r) * error_weight_max.powf(r);

        //let t = 16.0;
        //let error_weight = 128.0;

        let args = GlobalSASolverArgument::new(t, error_weight);
        //let args = NakayamaSASolverArgument::new(t, error_weight);
        solution = solver.improve(solution.clone(), args);

        if i % 1000 == 0 {
            println!("i={}, t={}, e={}, cost={}", i, t, error_weight, solver.mc.cost);
            println!("{}", solver.mc.solution);
        }
    }
}
