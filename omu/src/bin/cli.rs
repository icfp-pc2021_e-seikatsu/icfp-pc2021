use anyhow::Result;
use omu_solver::dot::generate_bonus_file;
use omu_solver::input::{read_problem, read_solution};
use omu_solver::svg::{to_svg_from_problem, to_svg_from_solution};
use std::fs;
use structopt::StructOpt;

#[derive(StructOpt)]
struct VisBonusOpt {
    #[structopt(short, long)]
    problems_dir_path: String,
}

async fn vis_bonus(opt: VisBonusOpt) -> Result<()> {
    let file = generate_bonus_file(opt.problems_dir_path)?;

    fs::write("./bonuses.dot", &file.dot)?;
    fs::write("./bonuses.json", &file.json)?;

    println!("Created bonuses.dot, bonuses.json in current directory.");
    Ok(())
}

#[derive(StructOpt)]
struct VisProblemOpt {
    #[structopt(short, long)]
    problem_path: String,
    #[structopt(short, long)]
    output_path: String,
}

async fn vis_problem(opt: VisProblemOpt) -> Result<()> {
    let problem = read_problem(&opt.problem_path)?;
    let svg = to_svg_from_problem(&problem)?;
    svg::save(&opt.output_path, &svg)?;
    println!("Created {}", opt.output_path);
    Ok(())
}

#[derive(StructOpt)]
struct VisSolutionOpt {
    #[structopt(short, long)]
    problem_path: String,
    #[structopt(short, long)]
    solution_path: String,
    #[structopt(short, long)]
    output_path: String,
}

async fn vis_solution(opt: VisSolutionOpt) -> Result<()> {
    let problem = read_problem(&opt.problem_path)?;
    let solution = read_solution(&opt.solution_path)?;
    let svg = to_svg_from_solution(&problem, &solution)?;
    svg::save(&opt.output_path, &svg)?;
    println!("Created {}", opt.output_path);
    Ok(())
}

#[derive(StructOpt)]
enum VisOpt {
    Problem(VisProblemOpt),
    Solution(VisSolutionOpt),
    Bonus(VisBonusOpt),
}

async fn vis(opt: VisOpt) -> Result<()> {
    match opt {
        VisOpt::Solution(opt) => vis_solution(opt).await,
        VisOpt::Problem(opt) => vis_problem(opt).await,
        VisOpt::Bonus(opt) => vis_bonus(opt).await,
    }
}

#[derive(StructOpt)]
struct ScoreOpt {
    #[structopt(short, long)]
    problem_path: String,
    #[structopt(short, long)]
    solution_path: String,
}

async fn score(opt: ScoreOpt) -> Result<()> {
    let problem = read_problem(opt.problem_path);
    let solution = read_solution(opt.solution_path);
    if let (Ok(problem), Ok(solution)) = (problem, solution) {
        println!("{}", solution.score(&problem));
    } else {
        println!("{}", -1);
    }
    Ok(())
}

#[derive(StructOpt)]
struct DownloadProblemsOpt {
    #[structopt(short, long)]
    output_path: String,
}

async fn download_problems(opt: DownloadProblemsOpt) -> Result<()> {
    let problems = omu_solver::download::download_problems().await?;
    fs::create_dir_all(&opt.output_path)?;

    for problem in &problems {
        let problem_path = format!("{}/{}.json", &opt.output_path, problem.problem);
        let json = serde_json::to_string(&problem)?;
        fs::write(&problem_path, json)?;
        println!("{}", &problem_path);
    }

    println!("Finished download to {}", &opt.output_path);
    println!("Problem count: {}", problems.len());
    Ok(())
}

#[derive(StructOpt)]
enum DownloadOpt {
    Problems(DownloadProblemsOpt),
}

async fn download(opt: DownloadOpt) -> Result<()> {
    match opt {
        DownloadOpt::Problems(opt) => download_problems(opt).await,
    }
}

#[derive(StructOpt)]
enum Opt {
    Vis(VisOpt),
    Score(ScoreOpt),
    Download(DownloadOpt),
}

#[tokio::main]
async fn main() -> Result<()> {
    let opt = Opt::from_args();
    match opt {
        Opt::Vis(opt) => vis(opt).await,
        Opt::Score(opt) => score(opt).await,
        Opt::Download(opt) => download(opt).await,
    }
}
