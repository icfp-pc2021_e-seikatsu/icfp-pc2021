use crate::geometry::{contains_point_in_polygon, distance, edge_distance};
use serde_derive::{Deserialize, Serialize};
use std::cmp::min;
use std::fmt;
use std::fmt::{Display, Formatter};

pub type Coordinate = i64;
pub type Index = usize;
pub type Point = (Coordinate, Coordinate);
pub type Segment = (Point, Point);
pub type Edge = (Index, Index);
pub type ProblemId = Index;

pub type Polygon = Vec<Point>;
pub type Hole = Polygon;
pub type Edges = Vec<Edge>;
pub type Vertices = Polygon;

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Figure {
    pub edges: Edges,
    pub vertices: Vertices,
}

const GLOBALIST: &'static str = "GLOBALIST";

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Bonus {
    pub bonus: String,
    pub problem: ProblemId,
    pub position: Point,
}

#[derive(Serialize, Deserialize, Debug, Clone, Eq, PartialEq)]
pub struct UseBonus {
    pub bonus: String,
    pub problem: ProblemId,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Problem {
    pub problem: ProblemId,
    pub hole: Hole,
    pub figure: Figure,
    pub epsilon: f64,
    pub bonuses: Vec<Bonus>,
    pub can_use_bonuses: Vec<UseBonus>,
}

impl Problem {
    pub fn threshold(&self) -> f64 {
        //! epsilon
        self.epsilon / 1_000_000.
    }

    pub fn can_use_globalist(&self) -> bool {
        self.can_use_bonuses.iter().any(|b| b.bonus == GLOBALIST)
    }
}

impl Display for Problem {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        writeln!(f, "{}", serde_json::to_string(&self).unwrap())
    }
}

#[derive(Serialize, Deserialize, Debug, Clone, Default)]
pub struct Solution {
    pub vertices: Vertices,
    pub bonuses: Option<Vec<UseBonus>>,
    // 管理用
    pub problem: Option<u32>,
    pub time: Option<String>,
    pub solver: Option<String>,
    pub score: Option<i64>,
    pub submit_id: Option<String>,
}

impl Solution {
    pub fn new(vertices: Vertices) -> Self {
        Solution {
            vertices,
            bonuses: None,
            problem: None,
            time: None,
            solver: None,
            score: None,
            submit_id: None,
        }
    }
}

impl Display for Solution {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        writeln!(f, "{}", serde_json::to_string(&self).unwrap())
    }
}

impl Solution {
    // 影響箇所(solver)を変えるのが面倒なので abs ありをそのまま置いとく
    pub fn edge_value(&self, problem: &Problem, edge: Edge) -> f64 {
        self.edge_value_without_abs(problem, edge).abs()
    }

    pub fn edge_value_without_abs(&self, problem: &Problem, edge: Edge) -> f64 {
        /*!
        デフォルトの edge 長との比率 (solution_edge_length / default_length - 1) を出す
        */
        let problem_dist = edge_distance(&problem.figure.vertices, edge) as f64;
        let solution_dist = edge_distance(&self.vertices, edge) as f64;
        solution_dist / problem_dist - 1.
    }

    pub fn use_globalist(&self) -> bool {
        if let Some(ref v) = self.bonuses {
            v.iter().any(|s| s.bonus == GLOBALIST)
        } else {
            false
        }
    }

    pub fn is_valid(&self, problem: &Problem) -> bool {
        // problem.hole 内に solution.vertices が含まれている
        for vertex in &self.vertices {
            if !contains_point_in_polygon(*vertex, &problem.hole) {
                return false;
            }
        }

        // solution の各エッジの長さが許容範囲内であること
        let threshold = problem.threshold();
        if self.use_globalist() {
            if !problem.can_use_globalist() {
                return false;
            }
            let mut error = 0.0;
            for &edge in &problem.figure.edges {
                error += self.edge_value(problem, edge);
            }
            if error > threshold * problem.figure.edges.len() as f64 {
                return false;
            }
        } else {
            for &edge in &problem.figure.edges {
                if self.edge_value(problem, edge) > threshold {
                    return false;
                }
            }
        }
        true
    }

    pub fn score_by_ignore_validity(&self, problem: &Problem) -> i64 {
        let mut dislike = 0;
        for &hole_pos in &problem.hole {
            let mut min_dislike = Coordinate::MAX;
            for &vertex in &self.vertices {
                min_dislike = min(min_dislike, distance(hole_pos, vertex));
            }
            dislike += min_dislike;
        }
        dislike
    }
    pub fn score(&self, problem: &Problem) -> i64 {
        if !self.is_valid(problem) {
            return -1;
        }
        self.score_by_ignore_validity(problem)
    }
}

#[test]
fn test_is_valid_solution() {
    use crate::input::{read_problem, read_solution};
    use glob::glob;

    for g in glob("../solutions/*/*.json").unwrap() {
        let solution_path = g.unwrap();
        if solution_path.to_str().unwrap() == "../solutions/2/20210711_143235.json" {
            continue;
        }
        let number = solution_path
            .parent()
            .unwrap()
            .file_name()
            .unwrap()
            .to_str()
            .unwrap();
        let problem_path = format!("../problems/{}.json", number);
        println!("{} {}", problem_path, solution_path.display());

        let problem = read_problem(problem_path).unwrap();
        let solution = read_solution(solution_path).unwrap();
        assert!(solution.is_valid(&problem));
    }
}
