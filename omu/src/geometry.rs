use crate::model::{Coordinate, Edge, Edges, Point, Polygon, Segment, Vertices};
use itertools::Itertools;

pub type F = f64;
pub type P = (F, F);
pub type S = (P, P);
pub type Po = Vec<P>;

pub fn from_point(point: Point) -> P {
    (point.0 as F, point.1 as F)
}

pub fn from_segment(segment: Segment) -> S {
    (from_point(segment.0), from_point(segment.1))
}

pub fn from_polygon(polygon: &Polygon) -> Po {
    polygon.iter().map(|p| (p.0 as F, p.1 as F)).collect_vec()
}

// ref: https://ei1333.github.io/luzhiled/snippets/geometry/template.html

fn eps() -> F {
    1e-8
}

fn eq(a: F, b: F) -> bool {
    (b - a).abs() < eps()
}

pub fn cross(point0: P, point1: P) -> F {
    point0.0 * point1.1 - point0.1 * point1.0
}

pub fn dot(point0: P, point1: P) -> F {
    point0.0 * point1.0 + point0.1 * point1.1
}

pub fn add(point0: P, point1: P) -> P {
    (point0.0 + point1.0, point0.1 + point1.1)
}

pub fn sub(point0: P, point1: P) -> P {
    (point0.0 - point1.0, point0.1 - point1.1)
}

pub fn abs(point0: P) -> F {
    point0.0.abs() + point0.1.abs()
}

pub fn norm(point0: P) -> F {
    point0.0 * point0.0 + point0.1 * point0.1
}

pub fn ccw(a: P, b: P, c: P) -> i32 {
    let b = sub(b, a);
    let c = sub(c, a);
    if cross(b, c) > eps() {
        return 1;
    }
    if cross(b, c) < -eps() {
        return -1;
    }
    if dot(b, c) < 0. {
        return 2;
    }
    if norm(b) < norm(c) {
        return -2;
    }
    0
}

pub fn intersect_ss(s0: S, s1: S) -> bool {
    let ta = (s1.0 .0 - s1.1 .0) * (s0.0 .1 - s1.0 .1) + (s1.0 .1 - s1.1 .1) * (s1.0 .0 - s0.0 .0);
    let t1 = (s1.0 .0 - s1.1 .0) * (s0.1 .1 - s1.0 .1) + (s1.0 .1 - s1.1 .1) * (s1.0 .0 - s0.1 .0);
    let tc = (s0.0 .0 - s0.1 .0) * (s1.0 .1 - s0.0 .1) + (s0.0 .1 - s0.1 .1) * (s0.0 .0 - s1.0 .0);
    let td = (s0.0 .0 - s0.1 .0) * (s1.1 .1 - s0.0 .1) + (s0.0 .1 - s0.1 .1) * (s0.0 .0 - s1.1 .0);
    ta * t1 < eps() && tc * td < eps()
}

pub fn intersect_ps(p: P, s: S) -> bool {
    // ccw(s.0, s.1, p) == 0
    abs(sub(s.0, p)) + abs(sub(s.1, p)) - abs(sub(s.1, s.0)) < eps()
}

fn projection(p: P, s: S) -> P {
    let t = dot(sub(p, s.0), sub(s.0, s.1)) / norm(sub(s.0, s.1));
    let p = sub(s.0, s.1);
    (s.0 .0 + t * p.0, s.0 .1 + t * p.1)
}

pub fn distance_ps(p: P, s: S) -> F {
    let r = projection(p, s);
    if intersect_ps(p, s) {
        return abs(sub(r, p));
    }
    let a = abs(sub(s.0, p));
    let b = abs(sub(s.1, p));
    if a < b {
        a
    } else {
        b
    }
}

pub fn cross_point_ss(s: S, t: S) -> P {
    let a = cross(sub(s.1, s.0), sub(t.1, t.0));
    let b = cross(sub(s.1, s.0), sub(s.1, t.0));

    if eq(a.abs(), 0.) && eq(b.abs(), 0.) {
        return t.0;
    }

    let p0 = sub(t.1, t.0);
    let t0 = t.0;
    let mag = b / a;
    (t0.0 + p0.0 * mag, t0.1 + p0.1 * mag)
}

// ref: http://www.prefield.com/algorithm/geometry/convex_hull.html
// 凸多角形を取得する
pub fn convex_hull(polygon: &Po) -> Po {
    let mut ps = polygon.clone();
    ps.sort_by(|a, b| a.partial_cmp(b).unwrap());

    let n = ps.len();
    let mut ch = vec![(0., 0.); 2 * n];

    let mut k = 0;
    let mut i = 0_usize;
    loop {
        while k >= 2 && ccw(ch[k - 2], ch[k - 1], ps[i]) <= 0 {
            k -= 1;
        }
        ch[k] = ps[i];
        k += 1;
        i += 1;
        if i >= n {
            break;
        }
    }

    let mut i = n - 2;
    let t = k + 1;
    loop {
        while k >= t && ccw(ch[k - 2], ch[k - 1], ps[i]) <= 0 {
            k -= 1;
        }
        ch[k] = ps[i];
        if i == 0 {
            break;
        }
        k += 1;
        i -= 1;
    }

    ch.resize(k, (0., 0.));
    ch
}

// ref: https://www.nttpc.co.jp/technology/number_algorithm.html
pub fn winding_number_algorithm(point: P, polygon: &Po) -> bool {
    let mut wn: i32 = 0;

    for p in polygon {
        if p == &point {
            return true;
        }
    }

    for idx in 0..polygon.len() {
        let coordinate = &polygon[idx];
        let x0 = coordinate.0;
        let y0 = coordinate.1;

        let jdx = (idx + 1) % polygon.len();

        let coordinate = &polygon[jdx];
        let x1 = coordinate.0;
        let y1 = coordinate.1;

        let tx = point.0;
        let ty = point.1;

        if y0 <= ty && y1 > ty {
            let vt = (ty - y0) / (y1 - y0);
            if tx < (x0 + (vt * (x1 - x0))) {
                wn += 1;
            }
        } else if y0 > ty && y1 <= ty {
            let vt = (ty - y0) / (y1 - y0);
            if tx < (x0 + (vt * (x1 - x0))) {
                wn -= 1;
            }
        }
    }

    wn.abs() % 2 == 1
}

pub fn winding_number_algorithm_for_vertices(point: P, vertices: &Po, edges: &Edges) -> bool {
    let mut wn: i32 = 0;

    for p in vertices {
        if p == &point {
            return true;
        }
    }

    for (idx, jdx) in edges {
        let coordinate = &vertices[*idx];
        let x0 = coordinate.0;
        let y0 = coordinate.1;

        let coordinate = &vertices[*jdx];
        let x1 = coordinate.0;
        let y1 = coordinate.1;

        let tx = point.0;
        let ty = point.1;

        if y0 <= ty && y1 > ty {
            let vt = (ty - y0) / (y1 - y0);
            if tx < (x0 + (vt * (x1 - x0))) {
                wn += 1;
            }
        } else if y0 > ty && y1 <= ty {
            let vt = (ty - y0) / (y1 - y0);
            if tx < (x0 + (vt * (x1 - x0))) {
                wn -= 1;
            }
        }
    }

    wn.abs() % 2 == 1
}

pub fn distance(point0: Point, point1: Point) -> Coordinate {
    (point0.0 - point1.0).pow(2) + (point0.1 - point1.1).pow(2)
}

pub fn edge_distance(vertices: &Vertices, edge: Edge) -> Coordinate {
    let idx = edge.0;
    let jdx = edge.1;

    let p0 = vertices[idx];
    let p1 = vertices[jdx];

    distance(p0, p1)
}

pub fn contains_point_in_polygon(point: Point, polygon: &Polygon) -> bool {
    let p = (point.0 as F, point.1 as F);
    let po = polygon.iter().map(|p| (p.0 as F, p.1 as F)).collect_vec();
    winding_number_algorithm(p, &po)
}

pub fn contains_point_in_vertices(point: Point, vertices: &Vertices, edges: &Edges) -> bool {
    let p = (point.0 as F, point.1 as F);
    let v = vertices.iter().map(|p| (p.0 as F, p.1 as F)).collect_vec();
    winding_number_algorithm_for_vertices(p, &v, edges)
}

pub fn is_cross(s0: Segment, s1: Segment) -> bool {
    let ta = (s1.0 .0 - s1.1 .0) * (s0.0 .1 - s1.0 .1) + (s1.0 .1 - s1.1 .1) * (s1.0 .0 - s0.0 .0);
    let t1 = (s1.0 .0 - s1.1 .0) * (s0.1 .1 - s1.0 .1) + (s1.0 .1 - s1.1 .1) * (s1.0 .0 - s0.1 .0);
    let tc = (s0.0 .0 - s0.1 .0) * (s1.0 .1 - s0.0 .1) + (s0.0 .1 - s0.1 .1) * (s0.0 .0 - s1.0 .0);
    let td = (s0.0 .0 - s0.1 .0) * (s1.1 .1 - s0.0 .1) + (s0.0 .1 - s0.1 .1) * (s0.0 .0 - s1.1 .0);
    ta * t1 < 0 && tc * td < 0
}

#[test]
fn test_basic() -> anyhow::Result<()> {
    assert_eq!(cross((1., 2.), (2., 4.)), 0.);
    assert_eq!(dot((1., 2.), (2., 4.)), 10.);
    assert_eq!(norm((1., 2.)), 5.);
    assert_eq!(norm((2., 4.)), 20.);

    assert_eq!(cross((100., 256.), (917., 134.)), -221352.);
    assert_eq!(dot((100., 256.), (917., 134.)), 126004.);
    assert_eq!(norm((100., 256.)), 75536.);
    assert_eq!(norm((917., 134.)), 858845.);

    Ok(())
}

#[test]
fn test_ccw() -> anyhow::Result<()> {
    let a = (518., 326.);
    let b = (263., 544.);
    let c = (100., 200.);
    assert_eq!(ccw(a, b, c), 1);
    assert_eq!(ccw(b, a, c), -1);
    assert_eq!(ccw(c, b, a), -1);
    Ok(())
}

#[test]
fn test_intersect() -> anyhow::Result<()> {
    assert!(intersect_ss(
        ((10., 10.), (20., 10.)),
        ((15., 0.), (15., 20.)),
    ));
    assert!(!intersect_ss(
        ((10., 10.), (20., 10.)),
        ((0., 0.), (5., 0.)),
    ));
    assert!(intersect_ss(
        ((10., 10.), (20., 10.)),
        ((0., 0.), (30., 30.)),
    ));
    assert!(intersect_ss(
        ((0., 10.), (10., 10.)),
        ((10., 10.), (20., 10.)),
    ));

    Ok(())
}

#[test]
fn test_cross_point() -> anyhow::Result<()> {
    assert_eq!(
        cross_point_ss(((0., 0.), (100., 100.)), ((100., 0.), (0., 100.))),
        (50., 50.)
    );

    Ok(())
}

#[test]
fn test_convex_hull() -> anyhow::Result<()> {
    assert_eq!(
        convex_hull(&vec![(0., 0.), (2., 2.), (4., 5.)]),
        vec![(0., 0.), (2., 2.), (4., 5.)]
    );
    Ok(())
}

#[test]
fn test_contains_point_in_polygon() -> anyhow::Result<()> {
    assert!(winding_number_algorithm(
        (0., 1.),
        &vec![(0., 0.), (0., 2.), (2., 2.), (2., 0.)],
    ));

    assert!(winding_number_algorithm(
        (5., 5.),
        &vec![(10., 0.), (0., 10.), (10., 20.), (20., 10.)],
    ));

    // assert!(winding_number_algorithm(
    //     (52., 14.),
    //     &vec![(0., 0.), (50., 9.), (68., 54.), (47., 64.), (0., 16.)]
    // ));
    Ok(())
}
