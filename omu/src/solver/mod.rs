use crate::model::{Problem, Solution};

pub mod fix_sa;
pub mod sa;

pub trait Solver<Arguments> {
    fn new(problem: &Problem, solution: &Solution) -> Self;
    fn improve(&mut self, solution: Solution, args: Arguments) -> Solution;
}
