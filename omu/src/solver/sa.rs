// nakayama/src/bin の移植
// データモデルを omu_solver に合わせたもの
use rand::Rng;
use rand_pcg::Mcg128Xsl64;

use crate::geometry::{contains_point_in_polygon, distance, is_cross};
use crate::model::{Coordinate, Point, Problem, Segment, Solution};
use crate::solver::Solver;

pub struct SA {
    problem: Problem,
    hole_segments: Vec<Segment>,
    edges: Vec<Vec<usize>>,
    distances_by_vertex: Vec<Vec<i64>>,
    hole_cost: Coordinate,
    pub cost: f64,
    best_cost: i64,
    pub solution: Solution,
    best_solution: Solution,
}

impl SA {
    fn new(problem: &Problem, best_cost: i64) -> Self {
        let mut hole_segments = vec![];

        for idx in 0..problem.hole.len() {
            let jdx = (idx + 1) % problem.hole.len();
            let from = problem.hole[idx];
            let to = problem.hole[jdx];
            hole_segments.push((from, to))
        }

        let mut distances_by_vertex = vec![vec![]; problem.figure.edges.len()];

        for &(idx, jdx) in &problem.figure.edges {
            let d = distance(problem.figure.vertices[idx], problem.figure.vertices[jdx]);
            distances_by_vertex[idx].push(d);
            distances_by_vertex[jdx].push(d);
        }

        let mut edges = vec![vec![]; problem.figure.vertices.len()];

        for &(idx, jdx) in &problem.figure.edges {
            edges[idx].push(jdx);
            edges[jdx].push(idx);
        }
        let solution = Solution::new(vec![(0, 0); problem.figure.vertices.len()]);
        let best_solution = solution.clone();
        SA {
            problem: problem.clone(),
            hole_segments,
            edges,
            distances_by_vertex,
            hole_cost: 0,
            cost: 0.0,
            best_cost,
            solution,
            best_solution,
        }
    }

    fn initialize(&mut self, solution: Solution) {
        self.solution = solution;
    }

    fn calc_error_cost(&self, s: usize, point: Point) -> f64 {
        let mut error = 0.0;
        for (&t, &od) in self.edges[s].iter().zip(self.distances_by_vertex[s].iter()) {
            let d = (1.0 - distance(point, self.solution.vertices[t]) as f64 / od as f64).abs();
            if d > self.problem.threshold() {
                error += d / self.problem.threshold();
            }
        }
        error
    }

    fn calc_all_error_cost(&self) -> f64 {
        let mut error = 0.0;
        for s in 0..self.solution.vertices.len() {
            error += self.calc_error_cost(s, self.solution.vertices[s]);
        }
        error / 2.0
    }

    fn calc_hole_cost(&self) -> Coordinate {
        let mut cost = 0;
        for &h in self.problem.hole.iter() {
            let m = self
                .solution
                .vertices
                .iter()
                .map(|v| distance(*v, h))
                .min()
                .unwrap();
            cost += m;
        }
        cost
    }

    fn is_edge_valid(&self, s: usize, point: Point) -> bool {
        if !contains_point_in_polygon(point, &self.problem.hole) {
            return false;
        }
        for &t in self.edges[s].iter() {
            let segment = (point, self.solution.vertices[t]);
            for &hole_segment in &self.hole_segments {
                if is_cross(hole_segment, segment) {
                    return false;
                }
            }
        }
        true
    }

    fn update_all_cost(&mut self, error_weight: f64) {
        self.hole_cost = self.calc_hole_cost();
        self.cost = self.calc_all_error_cost() * error_weight + self.hole_cost as f64;
    }

    fn mmc<R>(&mut self, rng: &mut R, beta: f64, error_weight: f64)
    where
        R: Rng,
    {
        let v_idx = rng.gen_range(0..self.solution.vertices.len());
        let p = self.solution.vertices[v_idx];
        let q = {
            let mut q = self.solution.vertices[v_idx];
            while q == self.solution.vertices[v_idx] {
                q.0 += rng.gen_range(-1..=1);
                q.1 += rng.gen_range(-1..=1);
            }
            q
        };
        if !self.is_edge_valid(v_idx, q) {
            return;
        }
        let error_cost_diff = self.calc_error_cost(v_idx, q)
            - self.calc_error_cost(v_idx, self.solution.vertices[v_idx]);

        self.solution.vertices[v_idx] = q;

        let hole_cost_diff = self.calc_hole_cost() - self.hole_cost;
        let cost_diff = error_cost_diff * error_weight + hole_cost_diff as f64;
        if cost_diff < 0.0 || rng.gen_bool((-cost_diff * beta).exp()) {
            self.hole_cost += hole_cost_diff;
            self.cost += cost_diff;
            let cost = self.cost.round() as i64;
            if cost < self.best_cost && self.calc_all_error_cost() == 0.0 {
                self.best_cost = cost;
                self.best_solution = self.solution.clone()
            }
        } else {
            self.solution.vertices[v_idx] = p;
        }
    }
}

pub struct NakayamaSASolver {
    rng: Mcg128Xsl64,
    pub mc: SA,
}

pub struct NakayamaSASolverArgument {
    temperature: f64,
    error_weight: f64,
}

impl NakayamaSASolverArgument {
    pub fn new(temperature: f64, error_weight: f64) -> Self {
        NakayamaSASolverArgument {
            temperature,
            error_weight,
        }
    }
}

impl Solver<NakayamaSASolverArgument> for NakayamaSASolver {
    fn new(problem: &Problem, _solution: &Solution) -> Self {
        let rng = rand_pcg::Mcg128Xsl64::new(999);
        // TODO ちゃんとベスト取る
        let mc = SA::new(problem, i64::MAX);

        NakayamaSASolver { rng, mc }
    }
    fn improve(&mut self, solution: Solution, args: NakayamaSASolverArgument) -> Solution {
        self.mc.initialize(solution);
        let beta = 1.0 / args.temperature;
        self.mc.update_all_cost(args.error_weight);
        for _ in 0..self.mc.problem.figure.vertices.len() {
            self.mc.mmc(&mut self.rng, beta, args.error_weight);
        }
        self.mc.solution.clone()
    }
}
