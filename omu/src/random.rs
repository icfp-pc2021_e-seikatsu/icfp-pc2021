use rand::distributions::uniform::SampleRange;
use rand::distributions::{uniform::SampleUniform, Distribution, Standard};
use rand::rngs::StdRng;
use rand::Rng;
use std::sync::Mutex;

thread_local! {
    static RNG: Mutex<StdRng> = Mutex::new({
        let rng: StdRng = rand::SeedableRng::from_seed([1; 32]);
        rng
    });
}

pub fn random<T>() -> T
where
    Standard: Distribution<T>,
{
    RNG.with(|a| a.lock().unwrap().gen())
}

pub fn random_range<T, R>(range: R) -> T
where
    Standard: Distribution<T>,
    R: SampleRange<T>,
    T: SampleUniform + PartialOrd,
{
    RNG.with(|a| a.lock().unwrap().gen_range(range))
}
