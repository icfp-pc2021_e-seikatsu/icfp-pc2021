use crate::model::{Bonus, Problem, ProblemId, UseBonus};
use crate::raw::RawProblem;
use anyhow::Result;
use std::collections::HashMap;

pub fn link_problems(problems: &[Problem]) -> Vec<Problem> {
    let mut bonuses_by_id: HashMap<ProblemId, Vec<(ProblemId, Bonus)>> = HashMap::new();
    for problem in problems {
        for bonus in &problem.bonuses {
            bonuses_by_id
                .entry(bonus.problem)
                .or_default()
                .push((problem.problem, bonus.clone()))
        }
    }
    let mut linked_problems = vec![];
    for problem in problems {
        if let Some(bonuses) = bonuses_by_id.get(&problem.problem) {
            let mut problem = problem.clone();
            for (from, bonus) in bonuses {
                problem.can_use_bonuses.push(UseBonus {
                    bonus: bonus.bonus.clone(),
                    problem: *from,
                });
            }
            linked_problems.push(problem);
        }
    }
    linked_problems
}

pub async fn download_problem(problem_id: ProblemId) -> Result<Option<Problem>> {
    let client = reqwest::Client::new();
    let api_key = "7ec48e69-ddb3-43d3-95c5-54396b198224";
    let url = format!("https://poses.live/api/problems/{}", problem_id);

    let response = client
        .get(&url)
        .header("Authorization", format!("Bearer {}", api_key))
        .send()
        .await?;

    if response.status().is_server_error() {
        // 微妙だが存在しない場合 500 が返ってくるので, その場合は見つからなかったことにする
        return Ok(None);
    }

    let problem = response.json::<RawProblem>().await?;

    Ok(Some(problem.to_problem(problem_id)))
}

pub async fn download_problems() -> Result<Vec<Problem>> {
    let mut next_problem_id = 1;
    let mut problems = vec![];

    'downloads: loop {
        let mut futures = vec![];
        for _ in 0..10 {
            futures.push(download_problem(next_problem_id));
            next_problem_id += 1;
        }
        let results = futures::future::join_all(futures).await;
        for result in results {
            if let Some(problem) = result? {
                problems.push(problem);
            } else {
                break 'downloads;
            }
        }
    }

    Ok(link_problems(&problems))
}
