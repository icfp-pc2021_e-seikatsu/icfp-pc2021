use crate::model::{Bonus, Figure, Hole, Problem, ProblemId};
use serde_derive::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct RawProblem {
    pub hole: Hole,
    pub figure: Figure,
    pub epsilon: f64,
    pub bonuses: Vec<Bonus>,
}

impl RawProblem {
    pub fn to_problem(&self, problem_id: ProblemId) -> Problem {
        Problem {
            problem: problem_id,
            hole: self.hole.clone(),
            figure: self.figure.clone(),
            epsilon: self.epsilon,
            bonuses: self.bonuses.clone(),
            can_use_bonuses: vec![],
        }
    }
}
