use crate::model::{Problem, Solution};
use anyhow::Result;
use std::fs;
use std::path::Path;

pub fn read_problem<P>(path: P) -> Result<Problem>
where
    P: AsRef<Path>,
{
    let input_str = fs::read_to_string(path)?;
    let input: Problem = serde_json::from_str(&input_str)?;
    Ok(input)
}

pub fn read_solution<P>(path: P) -> Result<Solution>
where
    P: AsRef<Path>,
{
    let output_str = fs::read_to_string(path)?;
    let output: Solution = serde_json::from_str(&output_str)?;
    Ok(output)
}

