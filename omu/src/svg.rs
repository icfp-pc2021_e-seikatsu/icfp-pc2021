use crate::model::{Point, Problem, Solution};
use anyhow::Result;
use std::cmp::max;
use svg::node::element::path::Data;
use svg::node::element::Path;
use svg::Document;

pub fn to_svg_from_problem(problem: &Problem) -> Result<Document> {
    let mut max_x = 0;
    let mut max_y = 0;

    for pos in &problem.hole {
        max_x = max(pos.0, max_x);
        max_y = max(pos.1, max_y);
    }

    let mut hole_data = Data::new();
    let mut iter = problem.hole.iter();

    if let Some(begin_pos) = iter.next() {
        hole_data = hole_data.move_to(*begin_pos);
    }
    for pos in iter {
        hole_data = hole_data.line_to(*pos);
    }

    hole_data = hole_data.close();

    let mut figure_data = Data::new();

    for edge in &problem.figure.edges {
        let idx = edge.0;
        let jdx = edge.1;
        let from = problem.figure.vertices[idx];
        let to = problem.figure.vertices[jdx];

        figure_data = figure_data.move_to(from);
        figure_data = figure_data.line_to(to);
    }

    figure_data = figure_data.close();

    let hole_path = Path::new()
        .set("fill", "none")
        .set("stroke", "black")
        .set("d", hole_data);

    let figure_path = Path::new()
        .set("fill", "none")
        .set("stroke", "white")
        .set("d", figure_data);

    let document = Document::new()
        .set("viewBox", (0, 0, max_x + 10, max_y + 10))
        .add(hole_path)
        .add(figure_path);

    Ok(document)
}

pub fn to_svg_from_solution(problem: &Problem, solution: &Solution) -> Result<Document> {
    let mut data = Data::new();

    let mut max_x = 0;
    let mut max_y = 0;

    for pos in &problem.hole {
        max_x = max(pos.0, max_x);
        max_y = max(pos.1, max_y);
    }

    let mut hole_data = Data::new();
    let mut iter = problem.hole.iter();

    if let Some(begin_pos) = iter.next() {
        hole_data = hole_data.move_to(*begin_pos);
    }
    for pos in iter {
        hole_data = hole_data.line_to(*pos);
    }

    hole_data = hole_data.close();

    for edge in &problem.figure.edges {
        let idx = edge.0;
        let jdx = edge.1;
        let from = solution.vertices[idx];
        let to = solution.vertices[jdx];

        data = data.move_to(from);
        data = data.line_to(to);
    }

    data = data.close();
    let hole_path = Path::new()
        .set("fill", "none")
        .set("stroke", "black")
        .set("d", hole_data);
    let path = Path::new()
        .set("fill", "none")
        .set("stroke", "white")
        .set("d", data);
    let document = Document::new()
        .set("viewBox", (0, 0, max_x + 10, max_y + 10))
        .add(hole_path)
        .add(path);

    Ok(document)
}

pub fn to_svg_from_points(problem: &Problem, points: Vec<Point>) -> Result<Document> {
    let mut data = Data::new();

    let mut max_x = 0;
    let mut max_y = 0;

    for pos in &problem.hole {
        max_x = max(pos.0, max_x);
        max_y = max(pos.1, max_y);
    }

    let mut hole_data = Data::new();
    let mut iter = problem.hole.iter();

    if let Some(begin_pos) = iter.next() {
        hole_data = hole_data.move_to(*begin_pos);
    }
    for pos in iter {
        hole_data = hole_data.line_to(*pos);
    }

    hole_data = hole_data.close();

    for pos in &points {
        data = data.move_to(*pos);
        data = data.line_to((pos.0 + 1, pos.1 + 1));
    }

    data = data.close();
    let hole_path = Path::new()
        .set("fill", "none")
        .set("stroke", "black")
        .set("d", hole_data);

    let path = Path::new()
        .set("fill", "none")
        .set("stroke", "white")
        .set("d", data);

    let document = Document::new()
        .set("viewBox", (0, 0, max_x + 10, max_y + 10))
        .add(hole_path)
        .add(path);

    Ok(document)
}
