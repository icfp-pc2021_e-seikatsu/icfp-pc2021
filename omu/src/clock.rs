use std::time::{Duration, Instant};

pub struct Clock {
    begin_time: Instant,
}

impl Clock {
    pub fn new() -> Clock {
        let time = Instant::now();
        Clock { begin_time: time }
    }

    pub fn get_elapsed_duration(&self) -> Duration {
        Instant::now() - self.begin_time
    }

    pub fn get_elapsed_secs(&self) -> u64 {
        self.get_elapsed_duration().as_secs()
    }

    pub fn get_elapsed_milli_secs(&self) -> u128 {
        self.get_elapsed_duration().as_millis()
    }

    pub fn get_elapsed_micro_secs(&self) -> u128 {
        self.get_elapsed_duration().as_micros()
    }

    pub fn get_elapsed_nano_secs(&self) -> u128 {
        self.get_elapsed_duration().as_nanos()
    }
}

impl Default for Clock {
    fn default() -> Self {
        Clock::new()
    }
}
