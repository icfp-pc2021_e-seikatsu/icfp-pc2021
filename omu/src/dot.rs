use crate::input::read_problem;
use crate::model::Bonus;
use anyhow::Result;
use itertools::Itertools;
use regex::Regex;
use serde_derive::{Deserialize, Serialize};
use std::fs;
use std::str::FromStr;

pub struct BonusFile {
    pub dot: String,
    pub json: String,
}

pub fn generate_bonus_file(problems_dir_path: String) -> Result<BonusFile> {
    let problem_paths = fs::read_dir(problems_dir_path)?
        .map(|entry| {
            if let Ok(entry) = entry {
                let path = entry.path();
                if path.is_file() {
                    return Some(path);
                }
            }
            None
        })
        .filter(|p| p.is_some())
        .collect_vec();

    #[derive(Serialize, Deserialize, Debug, Clone)]
    struct ProblemBonus {
        problem_id: u32,
        bonuses: Vec<Bonus>,
    }

    #[derive(Serialize, Deserialize, Debug, Clone)]
    struct Bonuses {
        problem_bonuses: Vec<ProblemBonus>,
    }

    let mut problem_bonuses = vec![];

    for problem_path in &problem_paths {
        if let Some(problem_path) = problem_path {
            let problem = read_problem(problem_path)?;
            let re = Regex::new(r".+/([0-9]+)\.json")?;
            if let Some(path_str) = problem_path.to_str() {
                if let Some(captures) = re.captures(path_str) {
                    let problem_id = captures.get(1).unwrap().as_str();
                    let problem_bonus = ProblemBonus {
                        problem_id: u32::from_str(problem_id)?,
                        bonuses: problem.bonuses,
                    };
                    problem_bonuses.push(problem_bonus);
                }
            }
        }
    }

    let mut dot = String::new();
    dot += format!("digraph G {{").as_str();

    for problem_bonus in &problem_bonuses {
        for bonus in &problem_bonus.bonuses {
            dot += format!(
                "{} -> {} [ label=\"{}\" ];",
                problem_bonus.problem_id, bonus.problem, bonus.bonus
            )
            .as_str();
        }
    }

    dot += format!("}}").as_str();
    let json = serde_json::to_string(&problem_bonuses)?;

    let file = BonusFile { dot, json };
    Ok(file)
}
