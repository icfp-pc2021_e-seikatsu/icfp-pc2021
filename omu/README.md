# omu


## コマンド

### 問題ダウンロード

```bash
$ cargo run -- download problems --output-path ./problems
...
Finished download to ./problems
Problem count: 103
```

### スコア計算

```bash
$ cargo run -- score --problem-path ../problems/1.json --solution-path ../solutions/1/20210710_124539.json
632
```

### SVG 出力

```bash
$ cargo run -- vis problem --problem-path ../problems/1.json --output-path p.svg
Created p.svg
$ cargo run -- vis solution --problem-path ../problems/1.json --solution-path ../solutions/1/20210710_124539.json --output-path s.svg
Created s.svg
```

### ボーナスに関する情報を出力する

```bash
$ cargo run -- vis bonus --problems-dir-path ../problems
$ cat bonuses.dot | docker run --rm -i nshine/dot > bonuses.png
```
