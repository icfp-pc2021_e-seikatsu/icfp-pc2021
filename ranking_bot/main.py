import os
import requests
from html.parser import HTMLParser


def create_table(keys, values):
    column_length = {}
    for k in keys:
        max_length = len(k)
        for v in values:
            max_length = max(max_length, len(str(v[k])))
        column_length[k] = max_length

    result = ''
    for k in keys:
        result += '| %s ' % k + ' ' * (
            column_length[k] - len(k))
    result += '|\n'
    for k in keys:
        result += '|:' + '-' * (column_length[k] + 1)
    result += '|\n'
    for v in values:
        for k in keys:
            result += '| %s ' % v[k] + ' ' * (
                column_length[k] - len(str(v[k])))
        result += '|\n'

    return result


class RankParser(HTMLParser):

    def __init__(self):
        super().__init__()
        self.tr = False
        self.td = False
        self.data = []

    def handle_starttag(self, tag, attrs):
        if tag == 'tr':
            self.data.append([])
            self.tr = True
        if tag == 'footer':
            self.tr = False

    def handle_data(self, data):
        if self.tr:
            self.data[-1].append(data)


r = requests.get('https://poses.live/teams')
parser = RankParser()
parser.feed(r.text)
parser.close()

msg = ''
results = []
for d in parser.data:
    if (d[0] in ('1', '2', '3', '4', '5', '6', '7', '8', '9', '10')
            or d[1] in ('chirijako', 'Gon the Fox')):
        results.append({
            'place': d[0],
            'team': d[1],
            'score': d[2],
        })

msg = f"""ランキングだよ
```
{create_table(['place', 'team', 'score'], results)}```"""

slack_url = os.environ['SLACK_URL']
requests.post(slack_url, json={
    'text': msg, 'channel': '#soc_icfp-pc2021',
    'icon_emoji': ':icfp-pc:', 'username': 'ranking_bot'})
