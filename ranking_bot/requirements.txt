certifi==2021.5.30
chardet==4.0.0
html-parser==0.2
idna==2.10
ply==3.11
requests==2.25.1
urllib3==1.26.6
