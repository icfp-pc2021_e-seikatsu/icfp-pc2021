import json
from pathlib import Path


def main():
    pp = []
    for path in Path('./problems').glob('*.json'):
        data = json.load(path.open())
        data['problem'] = int(path.name.split('.')[0])
        data['can_use_bonuses'] = []
        pp.append(data)
    pp.sort(key=lambda d: d['problem'])
    for data in pp:
        for b in data['bonuses']:
            pp[b['problem'] - 1]['can_use_bonuses'].append({
                'bonus': b['bonus'],
                'problem': data['problem'],
            })
    for data in pp:
        with (Path('problems') / f'{data["problem"]}.json').open('w') as f:
            json.dump(data, f, indent=2, separators=(',', ': '))


if __name__ == '__main__':
    main()
